<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 21/10/14
 * Time: 23:17
 */

namespace Application\Sonata\UserBundle\Controller;

use Application\Sonata\UserBundle\Entity\User;
use FOS\RestBundle\Util\Codes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ValidatorController extends Controller
{

    public function uniqueAction()
    {
        $email = $this->getRequest()->query->get('email');
        $em = $this->getDoctrine()->getManager();
        $find = $em->getRepository('ApplicationSonataUserBundle:User')->findOneBy(array('email'=>$email));
        if($find){
            return new JsonResponse(array('status'=>1));
        }else{
            return new JsonResponse(array('status'=>0));
        }

    }
} 