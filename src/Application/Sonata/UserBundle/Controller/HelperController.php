<?php


namespace Application\Sonata\UserBundle\Controller;


use Application\Sonata\UserBundle\Entity\User;
use FOS\RestBundle\Util\Codes;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class HelperController extends Controller
{
	public function updateActiveSupplierAction()
	{
		if (!$this->get('security.context')->isGranted('ROLE_CLIENT')) {
			throw new HttpException(Codes::HTTP_FORBIDDEN, 'Only CLIENTs can access this method');
		}

		if ((!$supplierId = $this->container->get('request')->get('supplier')) || !$this->container->get('request')->get('redirect', false)) {
			throw new HttpException(Codes::HTTP_BAD_REQUEST, 'Missing param');
		}

		$entityManager = $this->container->get('doctrine.orm.default_entity_manager');
		$repo = $entityManager->getRepository('ServiceCrmProductBundle:Supplier');

		if (null === ($supplier = $repo->find($supplierId))) {
			throw new HttpException(Codes::HTTP_BAD_REQUEST, 'Wrong param');
		}

		/** @var User $user */
		$user = $this->getUser();
		$user->setClientsActiveSupplier($supplier);
		$entityManager->flush();

		$redirectUrl = $this->container->get('request')->get('redirect');
		return new RedirectResponse($redirectUrl);
	}

} 