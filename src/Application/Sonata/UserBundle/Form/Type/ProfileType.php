<?php

/*
 * This file is part of the Sonata package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */

namespace Application\Sonata\UserBundle\Form\Type;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use ServiceCrm\ProductBundle\Entity\Supplier;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Sonata\UserBundle\Model\UserInterface;

class ProfileType extends AbstractType
{
    private $class;

	/** @var  ContainerInterface */
	private $container;

    /**
     * @param string $class The User class name
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add($builder->create('login_data', 'form', array('virtual' => false, 'inherit_data' => true))
                ->add('username', null, array(
                    'label'    => 'form.label_username',
                    'required' => true,
                    'attr'=>array('readonly'=>true)
                ))
                ->add('plainPassword', 'text', array(
                    'label'    => 'form.label_plane_password',
                    'required' => false,
                ))
            )
            ->add($builder->create('person_data', 'form', array('virtual' => false, 'inherit_data' => true))
                ->add(
                    'gender', 'sonata_user_gender', array(
                    'label'    => 'form.label_gender',
                    'required' => true,
                    'translation_domain' => 'SonataUserBundle',
                    'choices' => array(
                        UserInterface::GENDER_FEMALE => 'gender_female',
                        UserInterface::GENDER_MALE   => 'gender_male',
                    ))
                    )
                ->add('firstname', null, array(
                    'label'    => 'form.label_firstname',
                    'required' => true
                ))
                ->add('lastname', null, array(
                    'label'    => 'form.label_lastname',
                    'required' => true
                ))
                ->add('dateOfBirth', 'date', array(
                    'label'    => 'form.label_date_of_birth',
                    'required' => true,
                    'widget'   => 'single_text',
                    'format' => 'yyyy.MM.dd.',
                    'attr'=>array('class'=>'datepicker')
                ))
            )
            ->add($builder->create('contact_data', 'form', array('virtual' => false, 'inherit_data' => true))
                ->add('email', null, array(
                    'label'    => 'form.label_email',
                    'required' => true,
                    'attr'=>array('readonly'=>true)
                ))
                ->add('phone', null, array(
                    'label'    => 'form.label_phone',
                    'required' => false,
                    'constraints' => array(
                        new \ServiceCrm\ProfileBundle\Constraints\Phone(),
                    ),
                    'attr'=>array('placeholder'=>'xx-xxx-xxxx')
                ))
                ->add('postalCity',null, array(
                        'label'    => 'form.label_postal_city',
                        'required' => true
                    ))
                ->add('postal_zip', null, array(
                    'label'    => 'form.label_postal_zip',
                    'required' => true
                ))
                ->add('postal_address', null, array(
                    'label'    => 'form.label_postal_address',
                    'required' => true
                ))
            )
        ;

	    $securityContext = $this->container->get('security.context');
	    if ($securityContext->isGranted('ROLE_CLIENT')) {
		    $currentUser = $securityContext->getToken()->getUser();
		    /** @var \Application\Sonata\UserBundle\Entity\User $user */

		    $options = array(
			    'attr' => array('style' => 'width:100%'),
			    'required' => false,
			    'multiple' => true,
			    'label' => 'form.supplier',
			    'class' => 'ServiceCrmProductBundle:Supplier',
			    'query_builder' => function(EntityRepository $er) {
				    return $er->createQueryBuilder('u')
					    ->andWhere('u.isPublic = 1')
					    ->orderBy('u.name', 'ASC');
			    },
			    'translation_domain' => 'SonataUserBundle',
		    );
		    $readonlyChoices = array();
		    if (!$securityContext->isGranted('ROLE_SUPER_ADMIN')) {
				$visibleChoices = $currentUser->getClientsSuppliers()->filter(function(Supplier $supplier){
					return $supplier->getIsPublic();
				})->toArray();
			    //important: we must keep other choices even they are not visible for the current user
			    $readonlyChoices = array_diff($currentUser->getClientsSuppliers()->toArray(), $visibleChoices);
			    $readonlyChoices = array_filter($readonlyChoices);
		    }

		    $clientsSuppliersField = $builder->create('clientsSuppliers', 'entity', $options);

		    if (!$securityContext->isGranted('ROLE_SUPER_ADMIN')) {
			    $clientsSuppliersField->addEventListener(FormEvents::SUBMIT, function(FormEvent $event) use($readonlyChoices){
				    /** @var ArrayCollection $currentData */
				    $currentData = $event->getData() ? $event->getData() : new ArrayCollection();
				    foreach($readonlyChoices as $readonlyChoice) {
					    $currentData->add($readonlyChoice);
				    }
				    $event->setData($currentData);
			    });
		    }

		    $servicesForm = $builder->create('services', 'form', array('virtual' => false, 'inherit_data' => true));
		    $servicesForm->add($clientsSuppliersField);
		    $builder->add($servicesForm);
	    }
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'application_sonata_user_profile';
    }

	/**
	 * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
	 */
	public function setContainer($container)
	{
		$this->container = $container;
	}
}
