<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 11/05/14
 * Time: 07:53
 */

namespace Application\Sonata\UserBundle\Admin\Model;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use ServiceCrm\ProductBundle\Entity\Supplier;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\UserBundle\Admin\Model\UserAdmin as BaseType;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class UserAdmin extends BaseType
{

	/** @var  ContainerInterface */
	private $container;

	/**
	 * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
	 */
	public function setContainer($container)
	{
		$this->container = $container;
	}

	protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('crm_profile'); #Action gets added automaticly
	    $collection->remove('acl');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username')
            ->add('email')
            ->add('phone')
            ->add('groups')
            ->add('enabled', null, array('editable' => true))
            ->add('locked', null, array('editable' => true))
        ;

       /* if ($this->isGranted('ROLE_ALLOWED_TO_SWITCH')) {
            $listMapper
                ->add('impersonating', 'string', array('template' => 'SonataUserBundle:Admin:Field/impersonating.html.twig'))
            ;
        }*/
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
	    $roles = $this->getSubject() ? $this->getSubject()->getRoles() : array();
	    /** @var \Application\Sonata\UserBundle\Entity\User $currentUser */
	    $currentUser = $this->container->get('security.context')->getToken()->getUser();



	    $formMapper
            ->with('group.general_data', array('class' => 'col-md-6'))
            ->with('group.membership', array('class' => 'col-md-6'))
            ->with('group.profile', array('class' => 'col-md-6'))
        ;

        $formMapper
            ->with('group.general_data')
                ->add('username')
                ->add('plainPassword', 'text', array('required' => false))
                ->add('phone')
                ->add('email',null,array('attr'=>array('class'=>'email_unique')))
                ->add('postalZip')
                ->add('postalCity')
                ->add('postalAddress')
            ->end();
            //->with('group.postal_address')
            //->end();

	    $formMapper
            ->with('group.membership')
                ->add('groups', 'sonata_type_model', array('required' => true,'multiple' => true, 'attr'=>array('style'=>'width:100%')));


	    if (in_array('ROLE_SUPPLIER', $roles) || in_array('ROLE_SUPPLIER_ADMIN', $roles)) {
		    $options = array('attr' => array('style' => 'width:100%'));
		    if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
				$options['choices'] = array($currentUser->getSupplier());
		    }
		    $formMapper->add('supplier', null, $options);
	    }

	    if (in_array('ROLE_CLIENT', $roles)) {
		    $options = array('attr' => array('style' => 'width:100%'), 'required' => false,);
		    $readonlyChoices = array();
		    if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
			    $options['choices'] = array($currentUser->getSupplier());
			    //important: we must keep other choices even they are not visible for the current user
			    $readonlyChoices = array_diff($this->getSubject() ? $this->getSubject()->getClientsSuppliers()->toArray() : array(), $options['choices']);
		    }

		    $clientsSuppliersField = $formMapper->create('clientsSuppliers', null, $options);

		    if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
			    $clientsSuppliersField->addEventListener(FormEvents::SUBMIT, function(FormEvent $event) use($readonlyChoices){
				    /** @var ArrayCollection $currentData */
				    $currentData = $event->getData() ? $event->getData() : new ArrayCollection();
				    foreach($readonlyChoices as $readonlyChoice) {
					    $currentData->add($readonlyChoice);
				    }
				    $event->setData($currentData);
			    });
		    }
		    $formMapper->add($clientsSuppliersField);
	    }

	    $formMapper
            ->end();

	    $formMapper
            ->with('group.profile')
                ->add('gender', 'sonata_user_gender', array(
                'required' => true,
                'translation_domain' => $this->getTranslationDomain()))
                ->add('firstname', null, array('required' => true))
                ->add('lastname', null, array('required' => true))
                ->add('dateOfBirth', 'birthday', array(
                        'required' => false,
                        'format' => 'yyyy/MM/dd',
                        'attr'=>array('style'=>'width:300px', 'customattr' => 'customdata')))
            ->end()
        ;

        if ($this->getSubject()) {
            $formMapper
                ->with('Beállítások');

		        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
			        $formMapper
			        ->add('groups', 'sonata_type_model', array(
				        'required' => false,
				        'expanded' => true,
				        'multiple' => true
			        ))
			        ->add('realRoles', 'sonata_security_roles', array(
				        'label'    => 'form.label_roles',
				        'expanded' => true,
				        'multiple' => true,
				        'required' => false
			        ));
	            };

	            $formMapper
                    ->add('locked', null, array('required' => false))
                    ->add('expired', null, array('required' => false))
                    ->add('enabled', null, array('required' => false))
                    ->add('credentialsExpired', null, array('required' => false))
                ->end()
            ;
        }

    }


    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
            ->add('lastname')
            ->add('firstname')
            ->add('email')
            ->add('phone')
            ->add('postalCity')
            ->add('postalZip')
            ->add('postalAddress')
        ;
    }


    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->with('group.general_data')
            ->add('username')
            ->add('email')
            ->add('postalZip')
            ->add('postalAddress')
            ->end()
            ->with('Groups')
            ->add('groups')
            ->end()
            ->with('group.profile')
            ->add('dateOfBirth')
            ->add('firstname')
            ->add('lastname')
            ->add('gender')
            ->add('phone')
            ->end()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($user)
    {
        $this->getUserManager()->updateCanonicalFields($user);
        $this->getUserManager()->updatePassword($user);
    }

    /**
     * @return UserManagerInterface
     */
    public function getUserManager()
    {
        return $this->userManager;
    }


	protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
	{
//		parent::configureTabMenu($menu, $action, $admin);
		if (!$childAdmin && !in_array($action, array('edit', 'show'))) {
			return;
		}

		$admin = $this->isChild() ? $this->getParent() : $this;

		$id = $admin->getRequest()->get('id');



		$menu->addChild(
			$this->trans('sidemenu.link_edit'),
			array('uri' => $admin->generateUrl('edit', array('id' => $id)))
		);


		$children = $admin->getChildren();
		/** @var Admin $child */
		foreach ($children as $child) {
			/** @var $childAdmin Admin */
			if ($child->isGranted('LIST')) {
				$menuItem = $menu->addChild(
					$this->trans('sidemenu.link_'.strtolower($child->getClassnameLabel())),
					array('uri' => $child->generateUrl('list', array('id' => $id)))
				);

				$menuItem->setCurrent($child->getCurrentChild());
			}
		}
	}

	public function addChild(AdminInterface $child)
	{
		parent::addChild($child); // TODO: Change the autogenerated stub
	}

	public function createQuery($context = 'list')
	{
		$query = parent::createQuery($context);
		$user = $this->container->get('security.context')->getToken()->getUser();

		if (!$this->isGranted('ROLE_SUPER_ADMIN') && $this->isGranted('ROLE_SUPPLIER') && $user->getSupplier()) {
			$rootAliases = $query->getRootAliases();
			$rootAlias = current($rootAliases);

			$query
				->leftJoin(sprintf('%s.clientsSuppliers', $rootAlias), 'clientsSuppliers')
				->andWhere(sprintf('(clientsSuppliers.id = :supplier_id OR %s.supplier = :supplier_id)', $rootAlias))
				->setParameter(':supplier_id', $user->getSupplier()->getId());
		}


		return $query;
	}

	public function getAvailableSuppliers()
	{
		if (!$this->isGranted('ROLE_CLIENT')) {
			return array();
		} else {
			/** @var User $user */
			$user = $this->container->get('security.context')->getToken()->getUser();

			$suppliers = $user->getClientsSuppliers();
			$result = array();
			/** @var Supplier $supplier */
			foreach ($suppliers as $supplier) {
				if ($supplier->getIsPublic()) $result[] = $supplier;
			}

			return $result;
		}
	}

	/**
	 * @return \Application\Sonata\UserBundle\Entity\User
	 */
	public function getSubject()
	{
		return parent::getSubject();
	}

	public function isGranted($name, $object = null)
	{
		if ($object instanceof User && $name === 'EDIT') {
			return true;
		}
		return parent::isGranted($name, $object);
	}
}