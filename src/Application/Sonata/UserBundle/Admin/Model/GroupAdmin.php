<?php


namespace Application\Sonata\UserBundle\Admin\Model;


use Sonata\AdminBundle\Datagrid\ListMapper;

class GroupAdmin extends \Sonata\UserBundle\Admin\Entity\GroupAdmin
{

	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->addIdentifier('name')
			->add('roles', 'text', array(
				'template' => 'ApplicationSonataUserBundle:GroupAdmin:list_field_roles.html.twig',
			))
		;
	}
}