<?php

namespace Application\Sonata\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Entity\BaseUser as BaseUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * User
 *
 * @ORM\Table(name="fos_user_user")
 * @ORM\Entity
 * @UniqueEntity("email")
 * @UniqueEntity("username")
 */

class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", name="postal_city", length=127, nullable=true)
     */
    protected $postalCity;

    /**
     * @ORM\Column(type="string", name="postal_zip", length=127, nullable=true)
     */
    protected $postalZip;

    /**
     * @ORM\Column(type="string", name="postal_address", length=127, nullable=true)
     */
    protected $postalAddress;

    /**
     * @var \ServiceCrm\ProductBundle\Entity\Supplier
     *
     * @ORM\ManyToOne(targetEntity="ServiceCrm\ProductBundle\Entity\Supplier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="supplier_id", referencedColumnName="id")
     * })
     */
    protected $supplier;

	/**
	 * @var ArrayCollection
	 * @ORM\ManyToMany(targetEntity="ServiceCrm\ProductBundle\Entity\Supplier")
	 */
	protected $clientsSuppliers;

	/**
	 * @var \ServiceCrm\ProductBundle\Entity\Supplier
	 *
	 * @ORM\ManyToOne(targetEntity="ServiceCrm\ProductBundle\Entity\Supplier")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="clients_active_supplier_id", referencedColumnName="id")
	 * })
	 */
	protected $clientsActiveSupplier;

	public function __construct()
	{
		parent::__construct();

		$this->clientsSuppliers = new ArrayCollection();
	}

    /**
     * @return mixed
     */
    public function getPostalZip()
    {
        return $this->postalZip;
    }

    /**
     * @param mixed $postalZip
     */
    public function setPostalZip($postalZip)
    {
        $this->postalZip = $postalZip;
    }

    /**
     * @return mixed
     */
    public function getPostalAddress()
    {
        return $this->postalAddress;
    }

    /**
     * @param mixed $postalAddress
     */
    public function setPostalAddress($postalAddress)
    {
        $this->postalAddress = $postalAddress;
    }

    /**
     * @return \ServiceCrm\ProductBundle\Entity\Supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param \ServiceCrm\ProductBundle\Entity\Supplier $supplier
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;
    }

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getClientsSuppliers()
	{
		return $this->clientsSuppliers;
	}

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $clientsSuppliers
	 */
	public function setClientsSuppliers($clientsSuppliers)
	{
		$this->clientsSuppliers = $clientsSuppliers;
	}

	/**
	 * @return \ServiceCrm\ProductBundle\Entity\Supplier
	 */
	public function getClientsActiveSupplier()
	{
		$result = $this->clientsActiveSupplier;

		if (null === $result) {
			$suppliers = $this->getClientsSuppliers();
			if ($suppliers->count()) {
				$result = $suppliers->first();
			}
		}

		if (null !== $result) {
			if (!$this->getClientsSuppliers()->contains($result)) {
				$result = null;
			}
		}

		return $result;
	}

	/**
	 * @param \ServiceCrm\ProductBundle\Entity\Supplier $clientsActiveSupplier
	 */
	public function setClientsActiveSupplier($clientsActiveSupplier)
	{
		$this->clientsActiveSupplier = $clientsActiveSupplier;
	}

    /**
     * @return mixed
     */
    public function getPostalCity()
    {
        return $this->postalCity;
    }

    /**
     * @param mixed $postalCity
     */
    public function setPostalCity($postalCity)
    {
        $this->postalCity = $postalCity;
    }

    /**
     * Add clientsSuppliers
     *
     * @param \ServiceCrm\ProductBundle\Entity\Supplier $clientsSuppliers
     * @return User
     */
    public function addClientsSupplier(\ServiceCrm\ProductBundle\Entity\Supplier $clientsSuppliers)
    {
        $this->clientsSuppliers[] = $clientsSuppliers;

        return $this;
    }

    /**
     * Remove clientsSuppliers
     *
     * @param \ServiceCrm\ProductBundle\Entity\Supplier $clientsSuppliers
     */
    public function removeClientsSupplier(\ServiceCrm\ProductBundle\Entity\Supplier $clientsSuppliers)
    {
        $this->clientsSuppliers->removeElement($clientsSuppliers);
    }
}

