<?php


namespace ServiceCrm\AbstractFileStoreBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RelatedMediaItemType extends AbstractType
{

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
//		$builder->add('id', 'hidden');
	}

	/**
	 * Returns the name of this type.
	 *
	 * @return string The name of this type
	 */
	public function getName()
	{
		return 'related_media_item';
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'ServiceCrm\AbstractFileStoreBundle\Model\MediaRelation',
			'label' => false,
		));
	}
}