<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 12/06/14
 * Time: 11:55
 */

namespace ServiceCrm\AbstractFileStoreBundle\Form\Type;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UploadedFiles extends AbstractType
{
    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;

    private $uploaderHelper;

    public function setContainer(\Symfony\Component\DependencyInjection\Container $container)
    {
        $this->container = $container;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {

        $resolver->setDefaults(array(
            'entity_id'=>null,
            'files'=> null
        ));
    }

   /* public function getParent()
    {
        return 'choice';
    }*/

    public function getName()
    {
        return 'uploaded_files';
    }

    public function buildView(FormView $view, FormInterface $form, array $options){

        parent::buildView($view,$form,$options);

        $view->vars['entity_id'] = $options['entity_id'];
        $view->vars['files'] = $options['files'];
        $view->vars['uploader_helper'] = $this->uploaderHelper;

    }


    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view,$form,$options);
    }

    /**
     * @param mixed $uploaderHelper
     */
    public function setUploaderHelper($uploaderHelper)
    {
        $this->uploaderHelper = $uploaderHelper;
    }

} 