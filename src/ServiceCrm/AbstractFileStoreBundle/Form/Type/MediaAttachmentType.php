<?php


namespace ServiceCrm\AbstractFileStoreBundle\Form\Type;


use Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MediaAttachmentType extends AbstractType
{
	/** @var  ContainerInterface */
	private $container;


	public function getName()
	{
		return 'media_attachment';
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$fileInput = $builder->create('file', 'file', array(
			'label'=>'label.attach_file',
			'mapped' => false,
			"multiple" => true,
			'required' => false, 'attr' => array(
				"accept" => "image/*",
			)));

		$pool = $this->container->get('sonata.media.pool');
		$fileInput->addEventListener(FormEvents::SUBMIT, function(FormEvent $event) use ($pool){
			/** @var ArrayCollection $parentFormData */
			$parentFormData = $event->getForm()->getParent()->getData();
			$fileData = $event->getData();
			if (empty($fileData) || null === current($fileData)) return;


			if (!is_array($fileData)) $fileData = array($fileData);
			foreach ($fileData as $uploadedFile) {
				$newMedia = new Media();
				$newMedia->setProviderName('sonata.media.provider.file');
				$newMedia->setContext('default');
				$newMedia->setBinaryContent($uploadedFile);

				$provider = $pool->getProvider($newMedia->getProviderName());
				$provider->transform($newMedia);

				$parentFormData->add($newMedia);
			}
		});

		$builder->add($fileInput);
	}

	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'required'  => false,
			'label'     => false,
		));
	}

	/**
	 * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
	 */
	public function setContainer($container)
	{
		$this->container = $container;
	}

	public function buildView(FormView $view, FormInterface $form, array $options)
	{
		parent::buildView($view, $form, $options);
	}
}