<?php

namespace ServiceCrm\AbstractFileStoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File as HttpFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\EntityManager;

//"ServiceCrmAssistanceServiceBundle:RequiringCourierFile" =   "ServiceCrm\ProductBundle\Entity\RequiringCourierFile",
/**
 * File
 *
 * @Vich\Uploadable
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\Table(name="file")
 * @ORM\Entity(repositoryClass="\ServiceCrm\AbstractFileStoreBundle\Entity\FileRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\DiscriminatorColumn(name="file_model", type="string")
 * @ORM\DiscriminatorMap({
 * })
 */
class File
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="mime_type", type="string", length=255)
     */
    protected $mimeType;

    /**
     * @var array
     *
     * @ORM\Column(name="metadata", type="json_array")
     */
    protected $metadata;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer")
     */
    protected $size;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="mtime", type="datetime")
     */
    protected $mtime;

    /**
     * @Assert\File()
     * @Vich\UploadableField(mapping="cms_other_file", fileNameProperty="fileName")
     *
     * @var HttpFile $image
     */
    protected $file;

    /**
     * @ORM\Column(type="string", length=255, name="file_name")
     *
     * @var string $imageName
     */
    protected $fileName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="ServiceCrm\AbstractFileStoreBundle\Entity\Relation", mappedBy="file")
     */
    protected $relations;

    /**
     * @var
     */
    protected $fileId;

    /**
     * @var
     */
    protected $entityId;

    /**
     * @var @ORM\Column(type="boolean", name="temp", nullable=true)
     */
    protected $temp;

    /**
     * @var \Datetime $deletedAt
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    protected $deletedAt;

    public function __construct()
    {
        $this->relations = new ArrayCollection(array());
    }

    /**
     * Set name
     *
     * @param string $name
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set metadata
     *
     * @param array $metadata
     * @return File
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    
        return $this;
    }

    /**
     * Get metadata
     *
     * @return array 
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return File
     */
    public function setSize($size)
    {
        $this->size = $size;
    
        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set mtime
     *
     * @param \DateTime $mtime
     * @return File
     */
    public function setMtime(\DateTime $mtime)
    {
        $this->mtime = $mtime;
    
        return $this;
    }

    /**
     * Get mtime
     *
     * @return \DateTime 
     */
    public function getMtime()
    {
        return $this->mtime;
    }

    /**
     * Set fileKey
     *
     * @param string $fileKey
     * @return File
     */
    public function setFileKey($fileKey)
    {
        $this->fileKey = $fileKey;
    
        return $this;
    }

    /**
     * Get fileKey
     *
     * @return string 
     */
    public function getFileKey()
    {
        return $this->fileKey;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        if ($file instanceof HttpFile) {
            $this->setMimeType($file->getMimeType());
            $this->setMetadata(array());
            $this->setSize($file->getSize());
            $this->setName($file->getClientOriginalName());
            $this->setMtime(\DateTime::createFromFormat('U', $file->getMTime()));
            $this->setFileName($file->getFilename());
        }
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @param string $mimeType
     */
    public function setMimeType($mimeType)
    {
        $this->mimeType = $mimeType;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRelations()
    {
        return $this->relations;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection $relations
     */
    public function setRelations($relations)
    {
        $this->relations = $relations;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add relations
     *
     * @param \ServiceCrm\AbstractFileStoreBundle\Entity\Relation $relations
     * @return File
     */
    public function addRelation(\ServiceCrm\AbstractFileStoreBundle\Entity\Relation $relations)
    {
        $this->relations[] = $relations;
    
        return $this;
    }

    /**
     * Remove relations
     *
     * @param \ServiceCrm\AbstractFileStoreBundle\Entity\Relation $relations
     */
    public function removeRelation(\ServiceCrm\AbstractFileStoreBundle\Entity\Relation $relations)
    {
        $this->relations->removeElement($relations);
    }

    /**
     * @return mixed
     */
    public function getFileId()
    {
        return $this->fileId;
    }

    /**
     * @param mixed $fileId
     */
    public function setFileId($fileId)
    {
        $this->fileId = $fileId;
    }

    /**
     * @return mixed
     */
    public function getEntityId()
    {
        return $this->entityId;
    }

    /**
     * @param mixed $entityId
     */
    public function setEntityId($entityId)
    {
        $this->entityId = $entityId;
    }

    /**
     * @return mixed
     */
    public function getTemp()
    {
        return $this->temp;
    }

    /**
     * @param mixed $temp
     */
    public function setTemp($temp)
    {
        $this->temp = $temp;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     * @return File
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

}