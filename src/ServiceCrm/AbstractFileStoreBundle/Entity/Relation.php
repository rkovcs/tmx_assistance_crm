<?php

namespace ServiceCrm\AbstractFileStoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FileRelation
 *
 * @ORM\Table(name="file_relation")
 *
 * @ORM\Entity
 */
class Relation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="acl_object_id", type="integer")
     */
    private $aclObjectId;


    /**
     * @var File
     *
     * @ORM\ManyToOne(targetEntity="ServiceCrm\AbstractFileStoreBundle\Entity\File")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     * })
     */
    private $file;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set aclObjectId
     *
     * @param integer $aclObjectId
     * @return FileRelation
     */
    public function setAclObjectId($aclObjectId)
    {
        $this->aclObjectId = $aclObjectId;
    
        return $this;
    }

    /**
     * Get aclObjectId
     *
     * @return integer 
     */
    public function getAclObjectId()
    {
        return $this->aclObjectId;
    }

    /**
     * @return \ServiceCrm\AbstractFileStoreBundle\Entity\File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param \ServiceCrm\AbstractFileStoreBundle\Entity\File $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }
}