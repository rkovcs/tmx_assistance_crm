<?php
/**
 * @author b4zs
 */

namespace ServiceCrm\AbstractFileStoreBundle\Entity;

use Doctrine\ORM\EntityRepository;


class FileRepository extends EntityRepository
{

    public function getQueryForAclObjectId($aclObjectId)
    {
        return $this
            ->createQueryBuilder('file')
            ->join('file.relations', 'relation')
            ->andWhere('relation.aclObjectId = :object_id')
            ->setParameter('object_id', $aclObjectId);
    }

    public function getFile($fileId){
        $this->find($fileId);
    }

    public function softDelete($fileId)
    {
        $file = $this->find($fileId);
        if($file){
            $em = $this->getEntityManager();
            $em->remove($file);
            $em->flush();
            return true;
        }else{
            return false;
        }
    }
}