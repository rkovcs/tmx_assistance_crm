<?php
/**
 * @author b4zs
 */

namespace ServiceCrm\AbstractFileStoreBundle\Manager;

use ServiceCrm\AbstractFileStoreBundle\Entity\Relation;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use ServiceCrm\AbstractFileStoreBundle\Classes\AbstractBaseFileStore;
use Symfony\Component\DependencyInjection\Container;


class FileRelationManager extends AbstractBaseFileStore
{

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * @param FileModel $file
     * @param $object
     * @return Relation
     */
    public function createRelation($file, $object)
    {
        $objectIdentity = $this->getAclForObject($object);

        $relation = new Relation();
        $relation->setFile($file);
        $relation->setAclObjectId($objectIdentity->getId());

        return $relation;
    }

    /**
     * @param $object
     * @return \Symfony\Component\Security\Acl\Domain\Acl
     */
    public function getAclForObject($object,$create = true)
    {
        $identity = ObjectIdentity::fromDomainObject($object);


        try {
            return $this->getAclProvider()->findAcl($identity);
        } catch (AclNotFoundException $e) {
            if($create){
                return $this->getAclProvider()->createAcl($identity);
            }
        }
    }

    private function getAclProvider()
    {
        return $this->container->get('security.acl.provider');
    }

    public function getFilesQueryForObject($object)
    {
        $objectIdentity = $this->getAclForObject($object);

        return $this
            ->container
            ->get('doctrine')
            ->getRepository($this->getFileRepository())
            ->getQueryForAclObjectId($objectIdentity->getId());
    }




} 