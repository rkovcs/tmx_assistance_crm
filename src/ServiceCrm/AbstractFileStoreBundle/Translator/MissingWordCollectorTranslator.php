<?php


namespace ServiceCrm\AbstractFileStoreBundle\Translator;


use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Yaml\Yaml;

class MissingWordCollectorTranslator extends Translator
{
	private $missingFile = null;

	private $missing = array();


	public function trans($id, array $parameters = array(), $domain = null, $locale = null)
	{
		$result = parent::trans($id, $parameters, $domain, $locale);

		if (1 || $id === $result) {
			if (is_string($id)) {
				if (!isset($this->missing[$domain])) {
					$this->missing[$domain] = array();
				}

				if (!array_key_exists($id, $this->missing[$domain])) {
					$this->missing[$domain][$id] = $result;
					$this->saveMissing();
				}
			}

		}

		return $result;
	}

	public function __construct(ContainerInterface $container, MessageSelector $selector, $loaderIds = array(), array $options = array())
	{
		parent::__construct($container, $selector, $loaderIds, $options);
		$this->missingFile = $container->getParameter('kernel.cache_dir') . '_' . $this->getLocale() . '_missing_translation.txt';
		$this->loadMissing();
	}

	private function saveMissing()
	{
		file_put_contents($this->missingFile, Yaml::dump($this->missing));
	}

	private function loadMissing()
	{
		try {
			if (file_exists($this->missingFile)) {
				$this->missing = Yaml::parse(file_get_contents($this->missingFile));
			};
		} catch (\Exception $e) {
			$this->missing = array();
		}
	}
}