<?php
/**
 * Created by PhpStorm.
 * User: krTeam
 * Date: 26/04/14
 * Time: 00:09
 */

namespace ServiceCrm\AbstractFileStoreBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use \Symfony\Component\Security\Core\SecurityContextInterface;
use Sonata\AdminBundle\Show\ShowMapper;

class MultiUploadAdmin extends Admin
{

    protected function configureRoutes(RouteCollection $collection)
    {
        //$collection->remove('list');
        //$collection->remove('create');
    }


    private $securityContext;

    /**
     * @var
     */
    private $container;

    /**
     * @param mixed $securityContext
     */
    public function setSecurityContext($securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // define group zoning
        $formMapper
            ->with('group.files')
                ->add('file', 'file', array('required' => false,"attr" => array(
                    "accept" => "image/*",
                    "multiple" => "multiple",
                )))
            ->end();
        ;

    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($data)
    {
    }

    public function createQuery($context = 'list')
    {
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

    public function preUpdate($project)
    {
    }

} 