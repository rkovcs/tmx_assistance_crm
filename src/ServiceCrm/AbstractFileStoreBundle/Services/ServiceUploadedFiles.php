<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 12/06/14
 * Time: 15:25
 */

namespace ServiceCrm\AbstractFileStoreBundle\Services;


class ServiceUploadedFiles {


    private $container;

    public function getFiles($entityObject,$fileEntityClass)
    {
        /**
         * @var $em
         */
        $em = $this->container->get('doctrine')->getManager();
        $fileRelationManager = $this->container->get('service_crm.abstract_file_store.relation_manager');


        /**
         * @var $aclObject
         */
        $aclObject = ($entityObject)?$fileRelationManager->getAclForObject($entityObject, false):null;
        if($aclObject){
            $fileQb = $em->getRepository($fileEntityClass)->getFileList($aclObject->getId());
            return $fileQb->getQuery()->getResult();
        }
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

} 