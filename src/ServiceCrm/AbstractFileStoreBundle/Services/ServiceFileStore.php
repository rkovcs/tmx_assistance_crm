<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 30/01/14
 * Time: 16:35
 */

namespace ServiceCrm\AbstractFileStoreBundle\Services;

use ServiceCrm\AbstractFileStoreBundle\Entity\Relation;
use ServiceCrm\AbstractFileStoreBundle\Entity\File as FileModel;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;
use ServiceCrm\AbstractFileStoreBundle\Classes\AbstractBaseFileStore;
use Symfony\Component\DependencyInjection\Container;


class ServiceFileStore extends AbstractBaseFileStore {

    private $index = true;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        parent::__construct($container);
    }

    /**
     * @param $request
     * @return array|null
     */
    public function create($request)
    {
        $form = $this->getFileForm()->getForm();
        $form->handleRequest($request);
        $result = null;
        if ($form->isValid()) {
            /** @var FileModel $file */
            $file = $form->getData();
            if(!$file->getEntityId()){
                $file->setTemp(true);
            }

            $this->persistFile($file);

            $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');

            $path = $helper->asset($file, 'file');
            $result = array(
                'status'    => 'success',
                'id'        => $file->getId(),
                'mime_type' => $file->getMimeType(),
                'url'       => $path,
            );
            if($file->getEntityId()){
                $this->prepareFileRelation($file);
            }


        } else {
            $result['status'] = 'error';
            if ($form->getErrors()) $result['error'] = $form->getErrorsAsString();
        }
        return $result;
    }

    public function persistFile($file)
    {
        $objectManager = $this->container->get('doctrine')->getManager();
        $objectManager->persist($file);
        $objectManager->flush();
        return $file;
    }

    public function prepareFileRelation($file)
    {
        $objectManager = $this->container->get('doctrine')->getManager();
        $relationManager = $this->container->get('ServiceCrm.abstract_file_store.relation_manager');
        $relation = $relationManager->createRelation($file, $this->getOwnerEntityObject());
        $objectManager->persist($relation);
        $objectManager->flush();
        return $relation;
    }

    public function getFile($fileId){
        return $this->container->get('doctrine')
            ->getManager()
            ->getRepository('ServiceCrmAbstractFileStoreBundle:File')
            ->find($fileId);
    }

    /**
     * @param boolean $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }

    public function getFilePath($fieldName = 'file'){
        $helper = $this->container->get('vich_uploader.templating.helper.uploader_helper');
        return $helper->asset($this->fileModel, $fieldName);
    }
} 