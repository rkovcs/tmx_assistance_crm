<?php


namespace ServiceCrm\AbstractFileStoreBundle\Model;


interface MediaRelation 
{
	public function getMedia();
} 