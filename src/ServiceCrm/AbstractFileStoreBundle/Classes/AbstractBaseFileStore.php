<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 03/02/14
 * Time: 15:08
 */

namespace ServiceCrm\AbstractFileStoreBundle\Classes;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Acl\Exception\AclNotFoundException;

abstract class AbstractBaseFileStore {

    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    protected $container;

    /**
     * @var
     */
    protected $fileModel = null;

    /**
     * @var
     */
    protected $fileRepository;

    /**
     * @var
     */
    protected $ownerEntityObject;

    /**
     * @var
     */
    protected $formActionUrl = null;

    /**
     * @var null
     */
    protected $fileConstraint = null;

    /**
     * @var
     */
    protected $fileForm = null;

    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->setFileConstraint(new Assert\File(array(
            'maxSize'    => '8M',
            'mimeTypes' => array(
                'image/jpeg',
                'image/pjpeg',
                'image/jpeg',
                'image/jpeg',
                'image/pjpeg',
                'image/jpeg',
                'image/pjpeg',
                'image/jpeg',
                'image/pjpeg',
                'image/png',
                'image/gif',
                'application/pdf',
                'image/x-jps',
                'application/x-pdf',
            ),
        )));
    }

    /**
     * @return mixed
     */
    public function getFileModel()
    {
        return $this->fileModel;
    }

    /**
     * @param mixed $fileModel
     */
    public function setFileModel($fileModel)
    {
        $this->fileModel = $fileModel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getOwnerEntityObject()
    {
        return $this->ownerEntityObject;
    }

    /**
     * @param mixed $ownerEntityObject
     */
    public function setOwnerEntityObject($ownerEntityObject)
    {
        $this->ownerEntityObject = $ownerEntityObject;
        $this->fileRepository = get_class($this->ownerEntityObject);
    }

    /**
     * @param mixed $formActionUrl
     */
    public function setFormActionUrl($formActionUrl)
    {
        $this->formActionUrl = $formActionUrl;
    }

    /**
     * Base form
     * @return mixed
     */
    public function buildForm()
    {
        if($this->fileModel) {
            if(!$this->fileForm){
                $this->fileForm = $this->container->get('form.factory')
                    ->createNamedBuilder('', 'form', $this->fileModel, array(
                        'property_path'     => null,
                        'csrf_protection'   => false,
                        'action'            => $this->formActionUrl

                    ))
                    ->add('file_id','text')
                    ->add('entity_id','text')
                    ->add('file', 'file', array(
                        'constraints' => array(
                            $this->fileConstraint
                        ),
                        'required' => true,
                    ));
                $this->fileForm->setMethod('POST');
            }
            return $this;
        }else{
            throw new \Exception('File model not exist');
        }
    }

    /**
     * @return null
     */
    public function getFileConstraint()
    {
        return $this->fileConstraint;
    }

    /**
     * @param null $fileConstraint
     */
    private function setFileConstraint($fileConstraint)
    {
        $this->fileConstraint = $fileConstraint;

        $this->fileConstraint->mimeTypesMessage = 'Invalid file type.';
    }

    /**
     * @return mixed
     */
    public function getFileForm()
    {
        if($this->fileForm){
            return $this->fileForm;
        }else {
            throw new \Exception('Form object not exist. Call the method "buildForm()"');
        }
    }

    /**
     * @param mixed $fileForm
     */
    public function setFileForm($fileForm)
    {
        $this->fileForm = $fileForm;
    }

}