<?php


namespace ServiceCrm\ProductBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class NotebookProductType extends AbstractType
{

	/**
	 * Returns the name of this type.
	 *
	 * @return string The name of this type
	 */
	public function getName()
	{
		return 'product';
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('screen_size', 'text', array(
			'virtual' => true,
		));
	}
}