<?php


namespace ServiceCrm\ProductBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SmartPhoneProductType extends  AbstractType
{

	public function getName()
	{
		return 'product';
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder->add('price', 'number', array(
			'virtual' => true,
		));
	}
}