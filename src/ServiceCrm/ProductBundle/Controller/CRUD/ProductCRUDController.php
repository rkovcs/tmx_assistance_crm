<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 13/05/14
 * Time: 21:38
 */

namespace ServiceCrm\ProductBundle\Controller\CRUD;

use Application\Sonata\UserBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use ServiceCrm\ProductBundle\Admin\ProductAdmin;
use ServiceCrm\ProductBundle\Entity\SupplierManufacturer;
use Sonata\AdminBundle\Controller\CRUDController as CRUDController;
use Sonata\DoctrineORMAdminBundle\Tests\Filter\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;


class ProductCRUDController extends CRUDController {


    public function createAction()
    {
	    $request = $this->container->get('request');
	    /** @var User $user */
	    $user = $this->getUser();
	    /** @var ProductAdmin $admin */
	    $admin = $this->admin;
	    $type = $request->get('product_type', null);
	    $supplier = $request->get(
		    'supplier',
		    $admin->getSubjectUser() && $admin->getSubjectUser()->getClientsActiveSupplier() && ($user == $admin->getSubjectUser())
			    ? $admin->getSubjectUser()->getClientsActiveSupplier()->getId()
			    : null
	    );

		if ('GET' === $request->getMethod()) {
            if (!$supplier && ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN') || $this->get('security.context')->isGranted('ROLE_SUPPLIER_ADMIN'))) {
                $clientsSuppliers = $this->admin->getSubjectUser()->getClientsSuppliers();

                return $this->render('@ServiceCrmProduct/ProductAdmin/create_supplierselect.html.twig', array(
                    'suppliers' => $clientsSuppliers,
                    'admin' => $this->admin,
                    'admin_pool' => $this->container->get('sonata.admin.pool'),
                    'action' => 'create',
                    'object' => $this->admin->getNewInstance(),
                ));
            }
            if (!$type)  {
//                $typesRepository = $this->container->get('doctrine')->getRepository('ServiceCrmProductBundle:ProductType');
//	            /** @var QueryBuilder $typesQueryBuilder */
//                $typesQueryBuilder = $typesRepository->createQueryBuilder('productType');

	            $doctrine = $this->container->get('doctrine');
	            /** @var EntityRepository $smRepo */
	            $smRepo = $doctrine->getRepository('ServiceCrmProductBundle:SupplierManufacturer');
	            $queryBuilder = $smRepo
		            ->createQueryBuilder('sm')
	                ->select('sm', 'productType')
	                ->innerJoin('sm.productTypes', 'productType')
		            ->andWhere('sm.supplier = :supplier')
		            ->setParameter('supplier', $supplier);

	            $query = $queryBuilder->getQuery();
	            $query->execute();
	            $sms = $query->getResult();

	            $types = array();
	            /** @var SupplierManufacturer $sm */
	            foreach ($sms as $sm) {
		            $types = array_merge($types, $sm->getProductTypes()->toArray());
	            }

	            $types = array_unique($types);

//	            var_dump($query->getDQL(), $query->getParameters(),$types);die;

                return $this->render('@ServiceCrmProduct/ProductAdmin/create_typeselect.html.twig', array(
                    'types' => $types,
                    'admin' => $this->admin,
                    'admin_pool' => $this->container->get('sonata.admin.pool'),
                    'action' => 'create',
                    'object' => $this->admin->getNewInstance(),
                ));
            }
		} else {
			$this->admin->getNewInstance();
		}

        return parent::createAction();
    }

} 