<?php
/**
 * Created by PhpStorm.
 * User: krTeam
 * Date: 26/04/14
 * Time: 00:09
 */

namespace ServiceCrm\ProductBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use \Symfony\Component\Security\Core\SecurityContextInterface;

class PhoneAdmin extends Admin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        //$collection->remove('list');
        //$collection->remove('create');
    }


    private $securityContext;

    /**
     * @param mixed $securityContext
     */
    public function setSecurityContext($securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // define group zoning
        $formMapper
            ->with('group.general_data', array('class' => 'col-md-6'))
            ->with('group.guarantee', array('class' => 'col-md-6'))
            ->with('group.place_and_time', array('class' => 'col-md-6'));

        $formMapper
            ->with('group.general_data')
                ->add('manufacturer', 'entity', array(
                    'label' => 'label.manufacturer',
                    'class' => 'ServiceCrmProductBundle:Manufacturer',
                    'required' => true,
                    'property'=>'name',
                    'attr'=>array('style'=>'width:100%')                ))
                ->add('modelCode', 'text', array('label' => 'label.model_code'))
                ->add('productName','hidden',array('data'=>'phone'))
                ->add('type', 'text', array('label' => 'label.product_type'))
                ->add('serial', 'text', array('label' => 'label.serial'))
                ->add('imei', 'text', array('label' => 'label.imei','required'=>false))
            ->end()
            ->with('group.guarantee')
                ->add('guarantee', 'text', array('label' => 'label.guarantee','required'=>false))
                //->add('guaranteePlus', 'text', array('label' => 'label.guarantee_plus','required'=>false))
            ->end()
            ->with('group.place_and_time')
                ->add('buyPlace', 'text', array('label' => 'label.place_of_purchase'))
                ->add('buyDate', 'date', array('label' => 'label.place_of_date'))
            ->end()
        ;

    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('type')
            ->add('manufacturer')
            ->add('created',null,array())
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('type')
            ->add('manufacturer')
            ->add('created')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($data)
    {
        $user = $this->securityContext->getToken()->getUser();
        $data->setUser($user);
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);
        $query->andWhere(
            $query->getRootAlias() . '.user = :user'
        );
        $query->setParameter('user', $this->securityContext->getToken()->getUser());
        return $query;
    }

} 