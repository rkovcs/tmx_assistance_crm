<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 25/05/14
 * Time: 15:28
 */

namespace ServiceCrm\ProductBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use \Symfony\Component\Security\Core\SecurityContextInterface;

class SupplierManufacturerAdmin extends Admin{

    private $securityContext;

    /**
     * @param mixed $securityContext
     */
    public function setSecurityContext($securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('supplier', 'entity', array(
                'label' => 'label.supplier',
                'class' => 'ServiceCrmProductBundle:Supplier',
                'required' => true,
                'property'=>'name',
                'attr'=>array('style'=>'width:100%')))
            ->add('manufacturer', 'entity', array(
                'label' => 'label.manufacturer',
                'class' => 'ServiceCrmProductBundle:Manufacturer',
                'required' => true,
                'property'=>'name',
                'attr'=>array('style'=>'width:100%')))
           ->add('assistance', 'sonata_type_model',
                array(
                    'required' => true,
                    'multiple'=>true,
                    'attr'=>array('style'=>'width:100%'),
                )
            )
            ->add('productTypes', 'sonata_type_model', array(
		        'required' => true,
		        'multiple'=>true,
		        'attr'=>array('style'=>'width:100%'),
	        ))

        ;

    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('supplier',null,array('class'=>'ServiceCrmProductBundle:Supplier','property'=>'name'))
            ->add('manufacturer')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('supplier')
            ->add('manufacturer')
        ;
    }

} 