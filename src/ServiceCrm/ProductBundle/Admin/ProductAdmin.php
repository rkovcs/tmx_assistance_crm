<?php
/**
 * Created by PhpStorm.
 * User: krTeam
 * Date: 26/04/14
 * Time: 00:09
 */

namespace ServiceCrm\ProductBundle\Admin;

use Application\Sonata\MediaBundle\Entity\Media;
use Application\Sonata\UserBundle\Entity\User;
use Knp\Menu\ItemInterface as MenuItemInterface;
use ServiceCrm\AbstractFileStoreBundle\Form\Type\RelatedMediaItemType;
use ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierUser;
use ServiceCrm\AssistanceServiceBundle\Form\Type\RequiringCourierLinkType;
use ServiceCrm\ProductBundle\Entity\Product;
use ServiceCrm\ProductBundle\Entity\ProductHasMedia;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\MediaBundle\Form\DataTransformer\ProviderDataTransformer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpKernel\Exception\HttpException;
use \Symfony\Component\Security\Core\SecurityContextInterface;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormTypeInterface;
use Doctrine\ORM\EntityRepository;


class ProductAdmin extends Admin
{

    protected $context = 'product';

    protected function configureRoutes(RouteCollection $collection)
    {
        //$collection->remove('list');
        //$collection->remove('create');
        if($this->isGranted('ROLE_CLIENT')){
            $collection->remove('edit');
            $collection->remove('delete');
        }
        parent::configureRoutes($collection);
        $collection->remove('acl');
    }

    /*public function getTemplate($name)
    {

        //echo parent::getTemplate($name);exit;
    }*/


    private $securityContext;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param mixed $securityContext
     */
    public function setSecurityContext($securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // define group zoning
        $formMapper
            ->with('group.general_data', array('class' => 'col-md-6'))
            //->with('group.guarantee', array('class' => 'col-md-6'))
            ->with('group.place_and_time', array('class' => 'col-md-6'))
            ->with('group.files', array('class' => 'col-md-6'))
            ->with('group.assistances', array('class' => 'col-md-6'));

	    /** @var Product $subject */
        $subject = $this->getSubject();
        $entityObject = null;
        if($subject && $subject->getId()){
            $entityObject = $subject;
        }

	    $actualSupplier = $this->getActualSupplier();
        $formMapper->with('group.general_data');
        $formMapper
                ->add('manufacturer', 'entity', array(
                    'label' => 'label.manufacturer',
                    'class' => 'ServiceCrmProductBundle:Manufacturer',
                    'query_builder'=>function(EntityRepository $er)use($actualSupplier){
                            $queryBuilder = $er->createQueryBuilder('manuf')
                                ->select('manuf')
                                ->innerJoin('manuf.supplierManufacturer','sManuf')
                                /*->andWhere(
                                    'sManuf.supplier = ?1'
                                )*/
                                ->orderBy('manuf.name', 'ASC')
                                /*->setParameters(array(
                                    1=>$actualSupplier,
                                ))*/
                                ;

		                    return $queryBuilder;
                        },
                    'required' => true,
                    'property'=>'name',
                    'attr'=>array('style'=>'width:100%')
                ))
                ->add('type', 'text', array('label' => 'label.product_type'))
                ->add('modelCode', 'text', array('label' => 'label.model_code'))
                ->add('productName','hidden',array('data'=>'notebook'))
                ->add('serial', 'text', array('label' => 'label.serial'))
                ->add('imei', 'text', array('label' => 'label.imei','required'=>false))
            ->end()
            //->with('group.guarantee')
                //->add('guarantee', 'text', array('label' => 'label.guarantee','required'=>false))
                //->add('guaranteePlus', 'text', array('label' => 'label.guarantee_plus','required'=>false))
            //->end()
            ->with('group.place_and_time')
                ->add('buyPlace', 'text', array('label' => 'label.place_of_purchase', 'required'=>false))
                ->add('buyDate', 'date', array('label' => 'label.place_of_date','constraints' => array(

            ), 'widget' => 'single_text','format' => 'yyyy.MM.dd.', 'attr' => array('class' => 'datepicker')))
            ->end()
            ->with('group.files', array('collapsed' => true))
	            ->add('media', 'media_attachment', array('label' => false,))
            ->end();
        ;



	    $formMapper->with('group.assistances');

	    $fieldDesc = $this->getModelManager()->getNewFieldDescriptionInstance($this->getClass(), 'requiringCouriers');
	    $formMapper->add('requiringCouriers', 'collection', array(
			'type' => new RequiringCourierLinkType($fieldDesc, $this->getConfigurationPool()),
		    'required' => false,
	    ));

	    $formMapper->end();


	    if ($subject && $subject->getProductType() && $subject->getProductType()->getFormService()) {
		    $formService = $this->container->get($subject->getProductType()->getFormService());

		    $tmpBuilder = $this->container->get('form.factory');
		    $tmpBuilder = $tmpBuilder->createBuilder();

		    $formService->buildForm($tmpBuilder, $formMapper->getFormBuilder()->getOptions());

		    $fields = $tmpBuilder->all();

		    if (count($fields)) {

			    $formMapper->with('type_specific_data', array('class' => 'col-md-6'));

			    /** @var FormBuilder $field */
			    foreach ($fields as $field) {

				    $formMapper->add($field, $field->getType()->getName());
			    }


			    $formMapper->end();

		    }
	    }

    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $subject = $this->getSubject();
        $entityObject = null;
        if($subject && $subject->getId()){
            $entityObject = $subject;
        }

        $showMapper
            ->with('group.general_data')
                ->add('manufacturer')
                ->add('modelCode')
                ->add('productName')
                ->add('type')
                ->add('serial')
                ->add('imei')
            ->end()
            ->with('group.place_and_time')
                ->add('buyPlace')
                ->add('buyDate','date',array(
                    'format' => 'Y - h - d'
                ))
            ->end()
            ->with('group.show_files')
	            ->add('media', 'media_attachment', array('label' => false,))
            ->end()
            ->with('group.assistances')
                ->add('requiringCouriers')
            ->end()
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('type')
            ->add('manufacturer')
            ->add('created',null,array('field_options'=>array('attr'=>array('class' => 'datepicker'))))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('type')
            ->add('manufacturer')
            ->add('created')
        ;

	    if ($this->isGranted('ROLE_SUPPLIER') || $this->isGranted('ROLE_SUPPLIER_ADMIN')) {
		    $listMapper->add('user');
	    }
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($data)
    {
    }

	/**
	 * @return User
	 */
	public function getSubjectUser()
	{
		if ($this->isChild() && $parentAdmin = $this->getParent()) {
			$parentSubject = $parentAdmin->getSubject();
			if ($parentSubject instanceof User) {
				return $parentSubject;
			}
		}
		return $this->securityContext->getToken()->getUser();

	}

    private function getActualSupplier(){
        $activeSupplier = $this->getSubjectUser()->getClientsActiveSupplier();
        $supplier = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser()->getSupplier();
        $actualSupplier = null;
        if(($this->isGranted('ROLE_SUPPLIER') || $this->isGranted('ROLE_SUPPLIER_ADMIN')) && $supplier){
            $actualSupplier = $supplier;
        }elseif($this->isGranted('ROLE_CLIENT') && $activeSupplier){
            $actualSupplier = $activeSupplier;
        }

        return $actualSupplier;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        if(!$this->isGranted('ROLE_SUPER_ADMIN')){
            $query->andWhere($query->getRootAlias() . '.supplier = :user_supplier');
            $actualSupplier = $this->getActualSupplier();
            $query->setParameter('user_supplier', $actualSupplier);
        };
	    if (!$this->isGranted('ROLE_SUPPLIER') && !$this->isGranted('ROLE_SUPPLIER_ADMIN')) {
		    $query->andWhere($query->getRootAlias() . '.user = :user');
		    $query->setParameter('user', $this->getSubjectUser());
	    }else{
            $childId = $this->getRequest()->query->get('childId');
            if($childId){
                $query->andWhere($query->getRootAlias() . '.user = :user');
                $query->setParameter('user', $childId);
            }
        }

        return $query;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }

	public function getNewInstance()
	{
		/** @var Product $product */
		$product = parent::getNewInstance();

		$loggedInUser = $this->securityContext->getToken()->getUser();
		$product->setCreateUser($loggedInUser);
		$product->setUser($this->getSubjectUser());

		if ($typeId = $this->getRequest()->get('product_type')) {
			$typesRepository = $this->container->get('doctrine')->getRepository('ServiceCrmProductBundle:ProductType');
			$type = $typesRepository->find($typeId);
			if (null === $type) {
				throw new HttpException(400, 'Bad parameters');
			}

			$product->setProductType($type);
		}

        // Set product supplier
        if($loggedInUser && !$product->getSupplier()){
            $supplier = $this->getRequest()->query->get('supplier',null);
            $actualSupplier = null;
            if($supplier && $this->isGranted('ROLE_SUPER_ADMIN')){
                $actualSupplier = $this->container->get('doctrine')->getRepository('ServiceCrmProductBundle:Supplier')->find($supplier);
            }else{
                $actualSupplier = $this->getActualSupplier();
            }
            $product->setSupplier($actualSupplier);
        }

		return $product;
	}

	public function getPersistentParameters()
	{
		$params =  parent::getPersistentParameters();

		$params['product_type'] = $this->getSubject() && $this->getSubject()->getProductType() && !$this->getSubject()->getId() ? $this->getSubject()->getProductType()->getId() : null;

        $params['supplier'] = $this->hasRequest() && $this->getRequest()->get('supplier')
            ? $this->getRequest()->get('supplier')
            : ($this->getSubject() instanceof Product && $this->getSubject()->getSupplier() ? $this->getSubject()->getSupplier()->getId() : null);

		return $params;
	}

    public function isGranted($name, $object = null)
    {
        if($name == 'CREATE' && ($this->isGranted('ROLE_SUPPLIER') || $this->isGranted('ROLE_SUPPLIER_ADMIN')) && !$this->isChild())return false;
        return parent::isGranted($name, $object);
    }

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $admin = $this->isChild() ? $this->getParent() : $this;

        $menu->addChild(
            $this->trans('sidemenu.link_new_product'),
            array('uri' => $admin->generateUrl('create', array()))
        );

        if($this->getSubject()){
            $menu->addChild(
                $this->trans('sidemenu.link_add_assistance_service'),
                array('uri' => $this->container->get('router')->generate('admin_servicecrm_assistanceservice_requiringcourieruser_create',array('id'=>$this->getSubject()->getId())))
            );
        }

        parent::configureSideMenu($menu, $action, $childAdmin);
    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        parent::configureTabMenu($menu, $action, $childAdmin); // TODO: Change the autogenerated stub
    }
}