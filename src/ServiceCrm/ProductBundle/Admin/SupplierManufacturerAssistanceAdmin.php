<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 25/05/14
 * Time: 15:28
 */

namespace ServiceCrm\ProductBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use \Symfony\Component\Security\Core\SecurityContextInterface;

class SupplierManufacturerAssistanceAdmin extends Admin{

    private $securityContext;

    /**
     * @param mixed $securityContext
     */
    public function setSecurityContext($securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $formMapper
            ->add('assistance', 'entity', array(
                'label' => 'label.assistance',
                'class' => 'ServiceCrmAssistanceServiceBundle:Assistance',
                'required' => true,
                'property'=>'name',
                'attr'=>array('style'=>'width:100%'))
            );
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('assistance')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('assistance')
        ;
    }

} 