<?php
/**
 * Created by PhpStorm.
 * User: krTeam
 * Date: 26/04/14
 * Time: 00:09
 */

namespace ServiceCrm\ProductBundle\Admin;


use Doctrine\Common\Util\ClassUtils;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProductTypeAdmin extends Admin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('group.general_data')
                ->add('name')
                ->add('formService')
            ->end()
        ;
    }

	protected function configureListFields(ListMapper $list)
	{
		$list
			->addIdentifier('name')
			->add('formService', null, array('required' => false,));
	}
}