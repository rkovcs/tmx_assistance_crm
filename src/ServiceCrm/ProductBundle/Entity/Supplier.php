<?php

namespace ServiceCrm\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supplier
 *
 * @ORM\Table(name="supplier")
 * @ORM\Entity
 */
class Supplier
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=64, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=64, nullable=true)
     */
    private $email;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="is_public", type="boolean", nullable=true)
	 */
	private $isPublic;

    /**
     * @ORM\OneToMany(targetEntity="ServiceCrm\ProductBundle\Entity\SupplierManufacturer", mappedBy="supplier")
     */
    private $supplierManufacturers;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Supplier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Supplier
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Supplier
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    public function __toString(){

        return $this->name;

    }

	/**
	 * @return string
	 */
	public function getIsPublic()
	{
		return $this->isPublic;
	}

	/**
	 * @param string $isPublic
	 */
	public function setIsPublic($isPublic)
	{
		$this->isPublic = $isPublic;
	}
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->supplierManufacturers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add supplierManufacturer
     *
     * @param \ServiceCrm\ProductBundle\Entity\SupplierManufacturer $supplierManufacturers
     * @return Supplier
     */
    public function addSupplierManufacturer(\ServiceCrm\ProductBundle\Entity\SupplierManufacturer $supplierManufacturers)
    {
        $this->supplierManufacturers[] = $supplierManufacturers;

        return $this;
    }

    /**
     * Remove supplierManufacturer
     *
     * @param \ServiceCrm\ProductBundle\Entity\SupplierManufacturer $supplierManufacturers
     */
    public function removeSupplierManufacturers(\ServiceCrm\ProductBundle\Entity\SupplierManufacturer $supplierManufacturers)
    {
        $this->supplierManufacturers->removeElement($supplierManufacturers);
    }

    /**
     * Get supplierManufacturer
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupplierManufacturers()
    {
        return $this->supplierManufacturers;
    }
}
