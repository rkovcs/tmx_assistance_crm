<?php

namespace ServiceCrm\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use ServiceCrm\ProductBundle\Entity\ProductHasMedia;
/**
 * Product
 *
 * @ORM\Table(name="product_type")
 * @ORM\Entity()
 */
class ProductType{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="formService", type="string", length=255, nullable=true)
     */
    private $formService;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getFormService()
	{
		return $this->formService;
	}

	/**
	 * @param string $formService
	 */
	public function setFormService($formService)
	{
		$this->formService = $formService;
	}

	function __toString()
	{
		return $this->name ? $this->name : '';
	}
}
