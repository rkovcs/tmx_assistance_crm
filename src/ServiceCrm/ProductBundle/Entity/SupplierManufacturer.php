<?php

namespace ServiceCrm\ProductBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SupplierManufacturer
 *
 * @ORM\Table(name="supplier_manufacturer", indexes={@ORM\Index(name="IDX_576371852ADD6D8C", columns={"supplier_id"}), @ORM\Index(name="IDX_57637185A23B42D", columns={"manufacturer_id"})})
 * @ORM\Entity
 */
class SupplierManufacturer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \ServiceCrm\ProductBundle\Entity\Supplier
     *
     * @ORM\ManyToOne(targetEntity="ServiceCrm\ProductBundle\Entity\Supplier", inversedBy="supplierManufacturer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="supplier_id", referencedColumnName="id")
     * })
     */
    private $supplier;

    /**
     * @var \ServiceCrm\ProductBundle\Entity\Manufacturer
     *
     * @ORM\ManyToOne(targetEntity="ServiceCrm\ProductBundle\Entity\Manufacturer", inversedBy="supplierManufacturer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="manufacturer_id", referencedColumnName="id")
     * })
     */
    private $manufacturer;


    /**
     * @ORM\ManyToMany(targetEntity="ServiceCrm\AssistanceServiceBundle\Entity\Assistance", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="supplier_manufacture_assistance",
     *      joinColumns={@ORM\JoinColumn(name="supplier_manufacturer_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="assistance_id", referencedColumnName="id")}
     *      )
     */
    private $assistance;

	/**
	 * @ORM\ManyToMany(targetEntity="ServiceCrm\ProductBundle\Entity\ProductType", cascade={"persist", "remove"})
	 */
	private $productTypes;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set supplier
     *
     * @param \ServiceCrm\ProductBundle\Entity\Supplier $supplier
     * @return SupplierManufacturer
     */
    public function setSupplier(\ServiceCrm\ProductBundle\Entity\Supplier $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \ServiceCrm\ProductBundle\Entity\Supplier 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set manufacturer
     *
     * @param \ServiceCrm\ProductBundle\Entity\Manufacturer $manufacturer
     * @return SupplierManufacturer
     */
    public function setManufacturer(\ServiceCrm\ProductBundle\Entity\Manufacturer $manufacturer = null)
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return \ServiceCrm\ProductBundle\Entity\Manufacturer 
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }
    /**
     * Constructor
     */
    public function __construct()
    {}

    /**
     * Add assistance
     *
     * @param \ServiceCrm\AssistanceServiceBundle\Entity\Assistance $assistance
     * @return SupplierManufacturer
     */
    public function addAssistance(\ServiceCrm\AssistanceServiceBundle\Entity\Assistance $assistance)
    {
        $this->assistance[] = $assistance;

        return $this;
    }

    /**
     * Remove assistance
     *
     * @param \ServiceCrm\AssistanceServiceBundle\Entity\Assistance $assistance
     */
    public function removeAssistance(\ServiceCrm\AssistanceServiceBundle\Entity\Assistance $assistance)
    {
        $this->assistance->removeElement($assistance);
    }

    /**
     * Get assistance
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAssistance()
    {
        return $this->assistance;
    }

	/**
	 * @return ArrayCollection
	 */
	public function getProductTypes()
	{
		return $this->productTypes;
	}

	/**
	 * @param mixed $productTypes
	 */
	public function setProductTypes($productTypes)
	{
		$this->productTypes = $productTypes;
	}

    /**
     * Add productTypes
     *
     * @param \ServiceCrm\ProductBundle\Entity\ProductType $productTypes
     * @return SupplierManufacturer
     */
    public function addProductType(\ServiceCrm\ProductBundle\Entity\ProductType $productTypes)
    {
        $this->productTypes[] = $productTypes;

        return $this;
    }

    /**
     * Remove productTypes
     *
     * @param \ServiceCrm\ProductBundle\Entity\ProductType $productTypes
     */
    public function removeProductType(\ServiceCrm\ProductBundle\Entity\ProductType $productTypes)
    {
        $this->productTypes->removeElement($productTypes);
    }
}
