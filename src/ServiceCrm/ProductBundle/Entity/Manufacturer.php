<?php

namespace ServiceCrm\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Manufacturer
 *
 * @ORM\Table(name="manufacturer")
 * @ORM\Entity
 */
class Manufacturer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=64, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=128, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="ServiceCrm\ProductBundle\Entity\SupplierManufacturer", mappedBy="manufacturer", cascade={"all"})
     */
    private $supplierManufacturer;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Manufacturer
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Manufacturer
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Manufacturer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Manufacturer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Manufacturer
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Manufacturer
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    public function __toString(){

        return $this->name;

    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->supplierManufacturer = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add supplierManufacturer
     *
     * @param \ServiceCrm\ProductBundle\Entity\SupplierManufacturer $supplierManufacturer
     * @return Manufacturer
     */
    public function addSupplierManufacturer(\ServiceCrm\ProductBundle\Entity\SupplierManufacturer $supplierManufacturer)
    {
        $this->supplierManufacturer[] = $supplierManufacturer;

        return $this;
    }

    /**
     * Remove supplierManufacturer
     *
     * @param \ServiceCrm\ProductBundle\Entity\SupplierManufacturer $supplierManufacturer
     */
    public function removeSupplierManufacturer(\ServiceCrm\ProductBundle\Entity\SupplierManufacturer $supplierManufacturer)
    {
        $this->supplierManufacturer->removeElement($supplierManufacturer);
    }

    /**
     * Get supplierManufacturer
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupplierManufacturer()
    {
        return $this->supplierManufacturer;
    }
}
