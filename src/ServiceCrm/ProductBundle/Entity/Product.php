<?php

namespace ServiceCrm\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use ServiceCrm\ProductBundle\Entity\ProductHasMedia;
/**
 * Product
 *
 * @ORM\Table(name="product", indexes={@ORM\Index(name="IDX_D34A04AD3D0AE6DC", columns={"manufacturer"}), @ORM\Index(name="IDX_D34A04ADA76ED395", columns={"user_id"})})
 * @ORM\Entity(repositoryClass="ServiceCrm\ProductBundle\Entity\ProductRepository")
 */
class Product{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="modelCode", type="string", length=255, nullable=false)
     */
    private $modelcode;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="serial", type="string", length=255, nullable=false)
     */
    private $serial;

    /**
     * @var string
     *
     * @ORM\Column(name="imei", type="string", length=255, nullable=true)
     */
    private $imei;

    /**
     * @var string
     *
     * @ORM\Column(name="serial_check", type="string", length=255, nullable=true)
     */
    private $serialCheck;

    /**
     * @var string
     *
     * @ORM\Column(name="guarantee", type="string", length=255, nullable=true)
     */
    private $guarantee;

    /**
     * @var string
     *
     * @ORM\Column(name="guaranteePlus", type="string", length=255, nullable=true)
     */
    private $guaranteeplus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="guaranteeExpiryDate", type="datetime", nullable=true)
     */
    private $guaranteeexpirydate;

    /**
     * @var string
     *
     * @ORM\Column(name="buyPlace", type="string", length=255, nullable=true)
     */
    private $buyplace;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="buyDate", type="datetime", nullable=false)
     */
    private $buydate;

    /**
     * @var string
     *
     * @ORM\Column(name="productName", type="string", length=64, nullable=false)
     */
    private $productname;

    /**
     * @var \ServiceCrm\ProductBundle\Entity\Manufacturer
     *
     * @ORM\ManyToOne(targetEntity="ServiceCrm\ProductBundle\Entity\Manufacturer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="manufacturer", referencedColumnName="id")
     * })
     */
    private $manufacturer;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

	/**
	 * @var ProductType
	 *
	 * @ORM\ManyToOne(targetEntity="ServiceCrm\ProductBundle\Entity\ProductType")
	 */
	private $productType;


    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="create_user_id", referencedColumnName="id")
     * })
     */
    private $createUser;

	/**
	 * @var ArrayCollection
	 * @ORM\ManyToMany(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
	 * @ORM\JoinTable(name="product_media",
	 *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="media_id", referencedColumnName="id")}
	 * )
	 */
    private $media;

    /**
     * @var Supplier
     *
     * @ORM\ManyToOne(targetEntity="ServiceCrm\ProductBundle\Entity\Supplier")
     */
    private $supplier;

	/**
	 * @var integer
	 *
	 * @ORM\OneToMany(targetEntity="ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierBase", mappedBy="product")
	 *
	 */
	private $requiringCouriers;

    /**
     * @var
     */
    private $typeSerial;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Product
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return Product
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set modelcode
     *
     * @param string $modelcode
     * @return Product
     */
    public function setModelcode($modelcode)
    {
        $this->modelcode = $modelcode;

        return $this;
    }

    /**
     * Get modelcode
     *
     * @return string 
     */
    public function getModelcode()
    {
        return $this->modelcode;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Product
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set serial
     *
     * @param string $serial
     * @return Product
     */
    public function setSerial($serial)
    {
        $this->serial = $serial;

        return $this;
    }

    /**
     * Get serial
     *
     * @return string 
     */
    public function getSerial()
    {
        return $this->serial;
    }

    /**
     * Set imei
     *
     * @param string $imei
     * @return Product
     */
    public function setImei($imei)
    {
        $this->imei = $imei;

        return $this;
    }

    /**
     * Get imei
     *
     * @return string 
     */
    public function getImei()
    {
        return $this->imei;
    }

    /**
     * Set serialCheck
     *
     * @param string $serialCheck
     * @return Product
     */
    public function setSerialCheck($serialCheck)
    {
        $this->serialCheck = $serialCheck;

        return $this;
    }

    /**
     * Get serialCheck
     *
     * @return string 
     */
    public function getSerialCheck()
    {
        return $this->serialCheck;
    }

    /**
     * Set guarantee
     *
     * @param string $guarantee
     * @return Product
     */
    public function setGuarantee($guarantee)
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    /**
     * Get guarantee
     *
     * @return string 
     */
    public function getGuarantee()
    {
        return $this->guarantee;
    }

    /**
     * Set guaranteeplus
     *
     * @param string $guaranteeplus
     * @return Product
     */
    public function setGuaranteeplus($guaranteeplus)
    {
        $this->guaranteeplus = $guaranteeplus;

        return $this;
    }

    /**
     * Get guaranteeplus
     *
     * @return string 
     */
    public function getGuaranteeplus()
    {
        return $this->guaranteeplus;
    }

    /**
     * Set guaranteeexpirydate
     *
     * @param \DateTime $guaranteeexpirydate
     * @return Product
     */
    public function setGuaranteeexpirydate($guaranteeexpirydate)
    {
        $this->guaranteeexpirydate = $guaranteeexpirydate;

        return $this;
    }

    /**
     * Get guaranteeexpirydate
     *
     * @return \DateTime 
     */
    public function getGuaranteeexpirydate()
    {
        return $this->guaranteeexpirydate;
    }

    /**
     * Set buyplace
     *
     * @param string $buyplace
     * @return Product
     */
    public function setBuyplace($buyplace)
    {
        $this->buyplace = $buyplace;

        return $this;
    }

    /**
     * Get buyplace
     *
     * @return string 
     */
    public function getBuyplace()
    {
        return $this->buyplace;
    }

    /**
     * Set buydate
     *
     * @param \DateTime $buydate
     * @return Product
     */
    public function setBuydate($buydate)
    {
        $this->buydate = $buydate;

        return $this;
    }

    /**
     * Get buydate
     *
     * @return \DateTime 
     */
    public function getBuydate()
    {
        return $this->buydate;
    }

    /**
     * Set productname
     *
     * @param string $productname
     * @return Product
     */
    public function setProductname($productname)
    {
        $this->productname = $productname;

        return $this;
    }

    /**
     * Get productname
     *
     * @return string 
     */
    public function getProductname()
    {
        return $this->productname;
    }

    /**
     * Set manufacturer
     *
     * @param \ServiceCrm\ProductBundle\Entity\Manufacturer $manufacturer
     * @return Product
     */
    public function setManufacturer(\ServiceCrm\ProductBundle\Entity\Manufacturer $manufacturer = null)
    {
        $this->manufacturer = $manufacturer;
        return $this;
    }

    /**
     * Get manufacturer
     *
     * @return \ServiceCrm\ProductBundle\Entity\Manufacturer 
     */
    public function getManufacturer()
    {
        return $this->manufacturer;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Product
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function __toString()
    {
        return $this->type ? $this->type.' - '.$this->serial : '';

    }

    /**
     * @return \Application\Sonata\UserBundle\Entity\User
     */
    public function getCreateUser()
    {
        return $this->createUser;
    }

    /**
     * @param \Application\Sonata\UserBundle\Entity\User $createUser
     */
    public function setCreateUser($createUser)
    {
        $this->createUser = $createUser;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->media = new ArrayCollection();
    }



	/**
	 * @return \ServiceCrm\ProductBundle\Entity\ProductType
	 */
	public function getProductType()
	{
		return $this->productType;
	}

	/**
	 * @param \ServiceCrm\ProductBundle\Entity\ProductType $productType
	 */
	public function setProductType($productType)
	{
		$this->productType = $productType;
	}

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getMedia()
	{
		return $this->media;
	}

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $media
	 */
	public function setMedia($media)
	{
		$this->media = $media;
	}

    /**
     * Set supplier
     *
     * @param \ServiceCrm\ProductBundle\Entity\Supplier $supplier
     * @return Product
     */
    public function setSupplier(\ServiceCrm\ProductBundle\Entity\Supplier $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \ServiceCrm\ProductBundle\Entity\Supplier 
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Add media
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $media
     * @return Product
     */
    public function addMedia(\Application\Sonata\MediaBundle\Entity\Media $media)
    {
        $this->media[] = $media;

        return $this;
    }

    /**
     * Remove media
     *
     * @param \Application\Sonata\MediaBundle\Entity\Media $media
     */
    public function removeMedia(\Application\Sonata\MediaBundle\Entity\Media $media)
    {
        $this->media->removeElement($media);
    }

	/**
	 * @return int
	 */
	public function getRequiringCouriers()
	{
		return $this->requiringCouriers;
	}

	/**
	 * @param int $requiringCouriers
	 */
	public function setRequiringCouriers($requiringCouriers)
	{
		$this->requiringCouriers = $requiringCouriers;
	}

    /**
     * @return mixed
     */
    public function getTypeSerial()
    {
        return $this->type.' - '.$this->serial;
    }
}
