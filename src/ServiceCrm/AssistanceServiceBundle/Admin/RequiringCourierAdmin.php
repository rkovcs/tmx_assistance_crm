<?php
/**
 * Created by PhpStorm.
 * User: krTeam
 * Date: 26/04/14
 * Time: 00:09
 */

namespace ServiceCrm\AssistanceServiceBundle\Admin;

use Application\Sonata\MediaBundle\Entity\Media;
use Composer\Package\LinkConstraint\EmptyConstraint;
use Doctrine\ORM\QueryBuilder;
use FOS\UserBundle\Entity\User;
use Knp\Menu\ItemInterface as MenuItemInterface;
use ServiceCrm\AbstractFileStoreBundle\Form\Type\RelatedMediaItemType;
use ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierUser;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Mapper\BaseGroupedMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Constraints\Blank;
use Symfony\Component\Validator\Constraints\NotBlank;

class RequiringCourierAdmin extends Admin
{
	/** @var SecurityContext */
    private $securityContext;

	/** @var  ContainerInterface */
    private $container;

    /**
     * @param mixed $securityContext
     */
    public function setSecurityContext($securityContext)
    {
        $this->securityContext = $securityContext;
    }

    /**
     * @param mixed $container
     */
    public function setContainer($container)
    {
        $this->container = $container;
    }


    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
	    $this->configureFields($formMapper);

    }

	protected function configureFields(BaseGroupedMapper $formMapper)
    {
	    $subjectUser = $this->getSubjectUser();
        // define group zoning
        $formMapper
            ->with('Meghibásodott készülék adatai', array('class' => 'col-md-6'))
            ->with('Hová és mikor menjen a futár a készülékért?', array('class' => 'col-md-6'));

        $em = $this->container->get('doctrine')->getManager();
        $id = $this->getRequest()->get('id');
        $productObj = null;
        $baseRouteName = $this->getBaseRouteName();
        if($baseRouteName == 'admin_servicecrm_assistanceservice_requiringcourieruser'){
            $productObj = $this->getSubject()->getProduct();
        }elseif($baseRouteName == 'admin_sonata_user_user_requiringcourieruser'){
            $productObj = $this->getSubject()->getProduct();
        }
        $formMapper
            ->with('Meghibásodott készülék adatai')
                ->add('product', 'entity', array(
                    'label' => 'label.product',
                    'class' => 'ServiceCrm\ProductBundle\Entity\Product',
                    'query_builder'=>function(EntityRepository $er) use($subjectUser) {
                            return $er->createQueryBuilder('pr')
                                ->select('pr')
                                ->leftJoin('pr.manufacturer','manuf')
                                ->leftJoin('manuf.supplierManufacturer','sManuf')
                                ->leftJoin('sManuf.assistance','assistance')
                                ->andWhere('assistance.systemName = ?2')
                                ->andWhere(
                                    'pr.user = ?1'
                                )
                                ->orderBy('pr.type', 'ASC')
                                ->setParameters(array(
                                    1=>$subjectUser,
                                    2=>'requiring_courier'
                                ))
                                ;
                        },
                    'property'=>'typeSerial',
                    'data'=>$productObj,
                    'attr'=>array('style'=>'width:100%')
                ))
                ->add('accessories', 'text', array('label' => 'label.accessories', 'required'=>false))
                ->add('errorDescription', 'textarea', array(
			        'label' => 'label.error_description',
			        'required' => true,
		            'constraints' => array(
			            new NotBlank(),
		            ),
                ))
                //->add('replacementUnit', 'checkbox', array('label' => 'Kérek cserekészüléket', 'required'=>false))
            ->end()
            ->with('Hová és mikor menjen a futár a készülékért?')
                ->add('date', 'date', array(
                    'label' => 'label.date',
                    'widget' => 'single_text' ,
                    'attr'=>array('width'=>'100px','class'=>'datepicker'),
                    'format' => 'yyyy.MM.dd.',
                    'help'=>'A fenti dátum nem fix időpont. A futárt előzetes egyeztetés után tudjuk biztosítani!',
                    'constraints' => ($this->isGranted('ROLE_CLIENT'))?array(
                        new \ServiceCrm\AssistanceServiceBundle\Constraints\AssistanceRequiringDate(),
                    ):null,
                    )
                )
                ->add('zip', 'text', array('label' => 'label.zip'))
                ->add('city', 'text', array('label' => 'label.city'))
                ->add('address', 'text', array('label' => 'label.address'))
                ->add('phone', 'text', array(
		            'label' => 'label.phone',
		            'required' => true,
			        'constraints' => array(
				        new NotBlank(),
			        ),
	        ))
            ->end()
	        ->with('group.files', array('collapsed' => true))
				->add('media', 'media_attachment', array(
			        'label' => false
		        ))
	        ->end()
        ;
        if(!$this->isGranted('ROLE_CLIENT')){
            $formMapper->with('Felhasználó adatai')
                ->add('user.username', 'text', array(
                    'label' => 'label.username',
                    'required' => false,
                    'read_only'=> true,
                ))
                ->add('user.lastname', 'text', array(
                    'label' => 'label.lastname',
                    'required' => false,
                    'read_only'=> true,
                ))
                ->add('user.firstname', 'text', array(
                    'label' => 'label.firstname',
                    'required' => false,
                    'read_only'=> true,
                ))
                ->end();
        }
    }

	private function createFileInput(FormBuilder $formBuilder)
	{

	}

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('aliasUniqid')
            ->add('product')
            ->add('updated')
            ->add('date', 'doctrine_orm_date')
            ->add('assistanceStatus', null, array(), null, array(
		        'choices' => @$this->configurationPool->getContainer()->get('doctrine')->getRepository('ServiceCrmAssistanceServiceBundle:Assistance')->findOneBySystemName('requiring_courier')->getStatus(),
	        ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('prefixAliasUniqid')
            ->add('product')
            ->add('updated')
            ->add('assistanceStatus')
            /*->add('status',null,array(
                'template'=>'ServiceCrmAssistanceServiceBundle:Admin:list_requiring_courier_status.html.twig'
            ))*/
        ;

	    if ($this->isGranted('ROLE_SUPPLIER') || $this->isGranted('ROLE_SUPPLIER_ADMIN')) {
		    $listMapper->add('user');
	    }
    }

    /**
     * {@inheritdoc}
     */
    public function prePersist($data)
    {

    }

    private function setDefaultStatus($data){
        $repository = $this->container->get('doctrine')
            ->getRepository('ServiceCrmAssistanceServiceBundle:Assistance');
        $query = $repository->createQueryBuilder(
            'a'
        )
            ->join('a.status', 'aStatus')
            ->where('a.systemName = :system_name')
            ->setParameters(array('system_name'=>'requiring_courier'))
            ->getQuery();
        $result = $query->getResult();
        foreach($result AS $r){
            $status = $r->getStatus();
            foreach($status AS $s){
                if($s->getSort() == 1){
                    $data->setAssistanceStatus($s);
                    break;
                }
            }
            break;
        }
    }

	public function getSubjectUser()
	{
		if ($this->getSubject()) {
			return $this->getSubject()->getUser();
		}

		if ($this->isChild() && $parentAdmin = $this->getParent()) {
			$parentSubject = $parentAdmin->getSubject();
			if ($parentSubject instanceof User) {
				return $parentSubject;
			}
		}
		return $this->securityContext->getToken()->getUser();

	}


	public function getNewInstance()
	{
		/** @var RequiringCourierUser $record */
		$record =  parent::getNewInstance();
		$record->setUser($this->getSubjectUser());
		$record->setCreateUser($this->securityContext->getToken()->getUser());

        // Set uniqID
        if(!$record->getAliasUniqid()){
           $record->setAliasUniqid(rand(1000,1000000));
        }

        // Set product supplier
        if(!$record->getSupplier()){
            $supplier = $this->getRequest()->query->get('supplier',null);
            $actualSupplier = null;
            if($supplier && ($this->isGranted('ROLE_SUPER_ADMIN') || $this->isGranted('ROLE_SUPPLIER_ADMIN'))){
                $actualSupplier = $this->container->get('doctrine')->getRepository('ServiceCrmProductBundle:Supplier')->find($supplier);
            }else{
                $actualSupplier = $this->getActualSupplier();
            }
            $record->setSupplier($actualSupplier);
        }

        $this->setDefaultStatus($record);
        $record->setAssistance(
            $this->container->get('doctrine.orm.default_entity_manager')->getRepository('ServiceCrmAssistanceServiceBundle:Assistance')->findOneBy(array(
                'systemName'=>'requiring_courier'
            ))
        );
		return $record;
	}

	public function createQuery($context = 'list')
	{
		/** @var QueryBuilder $queryBuilder */
		$queryBuilder = parent::createQuery($context);

		$rootAliases = $queryBuilder->getRootAliases();
		$rootAlias = current($rootAliases);

        if(!$this->isGranted('ROLE_SUPER_ADMIN')){
            $queryBuilder->andWhere($queryBuilder->getRootAlias() . '.supplier = :user_supplier');
            $actualSupplier = $this->getActualSupplier();
            $queryBuilder->setParameter('user_supplier', $actualSupplier);
        }

		if (!$this->isGranted('ROLE_SUPPLIER') && !$this->isGranted('ROLE_SUPPLIER_ADMIN')) {
            $queryBuilder->andWhere($queryBuilder->getRootAlias() . '.user = :user');
            $queryBuilder->setParameter('user', $this->getSubjectUser());
		}else{
            $childId = $this->getRequest()->query->get('childId');
            if($childId){
                $queryBuilder->andWhere($queryBuilder->getRootAlias() . '.user = :user');
                $queryBuilder->setParameter('user', $childId);
            }
        }

		return $queryBuilder;
	}

	protected function configureShowFields(ShowMapper $formMapper)
	{
        $subjectUser = $this->getSubjectUser();
        // define group zoning
        $formMapper
            ->with('Meghibásodott készülék adatai', array('class' => 'col-md-6'))
            ->with('Hová és mikor menjen a futár a készülékért?', array('class' => 'col-md-6'));


        $em = $this->container->get('doctrine')->getManager();
        $id = $this->getRequest()->get('id');
        $formMapper
            ->with('Meghibásodott készülék adatai')
            ->add('product', 'entity', array(
                'label' => 'label.product',
                'class' => 'ServiceCrm\ProductBundle\Entity\Product',
                'query_builder'=>function(EntityRepository $er) use($subjectUser) {
                        return $er->createQueryBuilder('pr')
                            ->select('pr','manuf')
                            ->leftJoin('pr.manufacturer','manuf')
                            ->leftJoin('manuf.supplierManufacturer','sManuf')
                            ->leftJoin('sManuf.assistance','assistance')
                            ->andWhere('assistance.systemName = ?2')
                            ->andWhere(
                                'pr.user = ?1'
                            )
                            ->orderBy('pr.type', 'ASC')
                            ->setParameters(array(
                                1=>$subjectUser,
                                2=>'requiring_courier'
                            ))
                            ;
                    },
                'property'=>'typeSerial',
                'data'=>($id)?$em->getReference("ServiceCrmProductBundle:Product", $id):null,
                'attr'=>array('style'=>'width:100%')
            ))
            ->add('accessories', 'text', array('label' => 'label.accessories', 'required'=>false))
            ->add('errorDescription', 'textarea', array(
                'label' => 'label.error_description',
                'required' => true,
                'constraints' => array(
                    new NotBlank(),
                ),
            ))
            //->add('replacementUnit', 'checkbox', array('label' => 'Kérek cserekészüléket', 'required'=>false))
            ->end()
            ->with('Hová és mikor menjen a futár a készülékért?')
            ->add('date', 'date', array(
                    'label' => 'label.date',
                    'widget' => 'single_text' ,
                    'attr'=>array('width'=>'100px','class'=>'datepicker'),
                    'format' => 'Y-M-d.',
                    'constraints' => array(
                        new \ServiceCrm\AssistanceServiceBundle\Constraints\AssistanceRequiringDate(),
                    ),
                )
            )
            ->add('zip', 'text', array('label' => 'label.zip'))
            ->add('city', 'text', array('label' => 'label.city'))
            ->add('address', 'text', array('label' => 'label.address'))
            ->add('phone', 'text', array(
                'label' => 'label.phone',
                'required' => true,
                'constraints' => array(
                    new NotBlank(),
                ),
            ))
            ->end()
            ->with('group.files', array('collapsed' => true))
            ->add('media', 'media_attachment', array(
                'label' => false
            ))
            ->end();
            if(!$this->isGranted('ROLE_CLIENT')){
                $formMapper->with('Felhasználó adatai')
                    ->add('user.username', 'text', array(
                        'label' => 'label.username',
                        'required' => false,
                        'read_only'=> true,
                    ))
                    ->add('user.lastname', 'text', array(
                        'label' => 'label.lastname',
                        'required' => false,
                        'read_only'=> true,
                    ))
                    ->add('user.firstname', 'text', array(
                        'label' => 'label.firstname',
                        'required' => false,
                        'read_only'=> true,
                    ))
                    ->end();
            }
        ;
	}

	public function isGranted__($name, $object = null)
	{
//		if (is_a($object, $this->getClass())) {
//	        if ('EDIT' === $name && $object->getId()) return false;
//		}
		if($name == 'CREATE' && ($this->isGranted('ROLE_SUPPLIER') || $this->isGranted('ROLE_SUPPLIER_ADMIN')) && !$this->isChild())return false;

        if($name == 'CREATE' && !$this->isGranted('ROLE_SUPER_ADMIN')){
            $supplier = $this->getActualSupplier();
	        if (null !== $supplier) {
	            $supplerManufacturers = $supplier->getSupplierManufacturers();
	            if($supplerManufacturers){
	                foreach($supplerManufacturers AS $supplerManufacturer){
	                    $assistances = $supplerManufacturer->getAssistance();
	                    if($assistances){
	                        foreach($assistances AS $assistance){
	                            if($assistance->getSystemName() == 'requiring_courier'){
	                                return parent::isGranted($name, $object);
	                            }
	                        }
	                    }
	                }
	            }
	        }
            return false;
        }elseif($name == 'CREATE' && $this->isGranted('ROLE_SUPER_ADMIN') && !$this->isChild()){
            return false;
        }else{
            return parent::isGranted($name, $object);
        }
	}

    private function getActualSupplier(){
        $activeSupplier = $this->getSubjectUser()->getClientsActiveSupplier();
	    $supplier = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser()->getSupplier();
        $actualSupplier = null;
	    if(($this->isGranted('ROLE_SUPPLIER') || $this->isGranted('ROLE_SUPPLIER_ADMIN')) && $supplier){
            $actualSupplier = $supplier;
        }elseif($this->isGranted('ROLE_CLIENT') && $activeSupplier){
            $actualSupplier = $activeSupplier;
        }

        return $actualSupplier;
    }

	protected function configureRoutes(RouteCollection $collection)
	{
        //if(!$this->isGranted('ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_REQUIRING_COURIER_EDITOR')){
        if($this->isGranted('ROLE_CLIENT') && !$this->isGranted('ROLE_SUPER_ADMIN')){
            $collection->remove('edit');
            $collection->remove('delete');
            //$collection->remove('create');
        }

		parent::configureRoutes($collection);
		$collection->remove('acl');
	}

    protected function configureSideMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $admin = $this->isChild() ? $this->getParent() : $this;

        $menu->addChild(
            $this->trans('sidemenu.link_new_requiring_courier'),
            array('uri' => $admin->generateUrl('create', array()))
        );

        parent::configureSideMenu($menu, $action, $childAdmin);
    }
}
