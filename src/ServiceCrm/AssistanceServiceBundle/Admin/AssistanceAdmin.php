<?php
/**
 * Created by PhpStorm.
 * User: krTeam
 * Date: 26/04/14
 * Time: 00:09
 */

namespace ServiceCrm\AssistanceServiceBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class AssistanceAdmin extends Admin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array('label' => 'label.name'))
            ->add('systemName', 'text', array('label' => 'label.system_name','required'=>true))
            ->add('description', 'textarea', array('label' => 'label.description', 'required'=>false))
            ->add('status', 'sonata_type_collection',
                array(
                    'required' => true,
                    'attr'=>array('style'=>'width:100%'),
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                )
            )
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('systemName')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->add('systemName')
        ;
    }

	protected function configureRoutes(RouteCollection $collection)
	{
		$collection->remove('acl');
	}
}