<?php
/**
 * Created by PhpStorm.
 * User: krTeam
 * Date: 26/04/14
 * Time: 00:09
 */

namespace ServiceCrm\AssistanceServiceBundle\Admin;

use Application\Sonata\UserBundle\Entity\User;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Mapper\BaseGroupedMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class RequiringCourierAdministratorAdmin extends Admin
{
    private $securityContext;

    public function configureRoutes(RouteCollection $collection){
        $collection->clearExcept(array('list','edit', 'show'));
    }
    /**
     * @param mixed $securityContext
     */
    public function setSecurityContext($securityContext)
    {
        $this->securityContext = $securityContext;
    }


	/**
	 * @param FormMapper $formMapper
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$this->configureFields($formMapper);

	}

	protected function configureFields(BaseGroupedMapper $formMapper)
	{
        $subject = $this->getSubject();
        if($subject){
            // define group zoning
            $formMapper
                ->with('Meghibásodott készülék adatai', array('class' => 'col-md-6'))
                ->with('Átvétel helye, időpontja, elérhetőség', array('class' => 'col-md-6'));
            $formMapper
                ->with('Meghibásodott készülék adatai')
                ->add('assistanceStatus', 'entity', array(
                    'label' => 'label.status',
                    'class' => 'ServiceCrm\AssistanceServiceBundle\Entity\AssistanceStatus',
                    'query_builder'=>function(EntityRepository $er)use($subject) {
                            return $er->createQueryBuilder('aStatus')
                                ->select('aStatus')
                                ->leftJoin('aStatus.assistance','a')
                                ->andWhere('a.id = ?1')
                                ->orderBy('aStatus.name', 'ASC')
                                ->setParameters(array(
                                    1 => $subject->getAssistance() ? $subject->getAssistance()->getId() : null,
                                ))
                                ;
                            },
                    'property'=>'name',
                    'attr'=>array('style'=>'width:100%')
                ))
                ->add('product', 'entity', array(
                    'label' => 'label.product',
                    'class' => 'ServiceCrm\ProductBundle\Entity\Product',
                    'property'=>'typeSerial',
                    'read_only'=>true,
                    'attr'=>array('style'=>'width:100%')
                ))
                ->add('accessories', 'text', array('label' => 'label.accessories', 'required'=>false, 'attr'=>array(
                    'readonly'=>true
                )))
                ->add('errorDescription', 'textarea', array('label' => 'label.error_description', 'attr'=>array(
                    'readonly'=>true
                )))
                /*->add('replacementUnit', 'checkbox', array(
                    'label' => 'Kell cserekészülék',
                    'required'=>true,
                    'attr'=>array(
                        'readonly'=>false
                    )))*/
                ->end()
                ->with('Átvétel helye, időpontja, elérhetőség')
                ->add('zip', 'text', array('label' => 'label.zip'))
                ->add('city', 'text', array('label' => 'label.city'))
                ->add('address', 'text', array('label' => 'label.address'))
                ->add('date', 'date',
                    array(
                        'label' => 'label.date',
                        'format' => 'yyyy.MM.dd.',
                        'widget' => 'single_text',
                        'attr'=>array('class'=>'datepicker'))
                )

                ->add('phone', 'text', array('label' => 'label.phone'))
                ->end()
                ->with('Felhasználó adatai')
                ->add('user.username', 'text', array(
                    'label' => 'label.username',
                    'required' => false,
                    'read_only'=> true,
                ))
                ->add('user.lastname', 'text', array(
                    'label' => 'label.lastname',
                    'required' => false,
                    'read_only'=> true,
                ))
                ->add('user.firstname', 'text', array(
                    'label' => 'label.firstname',
                    'required' => false,
                    'read_only'=> true,
                ))
                ->end()
                ->with('Adminisztráció')
                    ->add('adminComment', 'textarea', array('label' => 'label.admin_comment', 'required' => false, 'attr'=>array(

                    )))
                    ->add('returnAddressZip', 'text', array('label' => 'label.return_zip','required' => false))
                    ->add('returnAddress', 'text', array('label' => 'label.return_address', 'required' => false))
                    ->add('returnDate', 'date', array('label' => 'label.return_date', 'required' => false, 'widget' => 'single_text', 'format' => 'yyyy.MM.dd.', 'attr'=>array(
                        'class' => 'datepicker'
                    )))
                ->end()
	            ->with('group.files', array('collapsed' => true))
		            ->add('media', 'media_attachment', array(
			            'label' => false
		            ))
	            ->end();
        }
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('aliasUniqid')
            ->add('product')
            ->add('updated')
            ->add('date',null,array('field_options'=>array('attr'=>array('class' => 'datepicker'))))
	        ->add('assistanceStatus', null, array(), null, array(
		        'choices' => @$this->configurationPool->getContainer()->get('doctrine')->getRepository('ServiceCrmAssistanceServiceBundle:Assistance')->findOneBySystemName('requiring_courier')->getStatus(),
	        ))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('prefixAliasUniqid')
            ->add('product')
            ->add('updated')
            ->add('date')
            ->add('assistanceStatus.name')
            /*->add('status','string',array(
                'template'=>'ServiceCrmAssistanceServiceBundle:Admin:list_requiring_courier_status.html.twig'
            ))*/
        ;
	    if ($this->isGranted('ROLE_SUPPLIER') || $this->isGranted('ROLE_SUPPLIER_ADMIN')) {
		    $listMapper->add('user');
	    }
    }


    /**
     * {@inheritdoc}
     */
    public function prePersist($data)
    {
        $user = $this->securityContext->getToken()->getUser();
        $data->setUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function preUpdate($data)
    {

    }

    public function getTemplate($name)
    {
        if($name == 'edit'){
            return 'ServiceCrmAssistanceServiceBundle:CRUD:administrator_edit.html.twig';
        }else{
            return parent::getTemplate($name);
        }
    }

    public function getAssistanceLogs(){
        $logs =  $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager()
            ->getRepository('Gedmo\Loggable\Entity\LogEntry')
            ->getLogEntries(
                $this->getSubject()
            );
        return $logs;
    }

    public function getStatusName($statusId){
        $status = $this->getConfigurationPool()->getContainer()->get('doctrine')->getManager()->getRepository('ServiceCrmAssistanceServiceBundle:AssistanceStatus')->find($statusId);
        if($status){
            return $status->getName();
        }else{
            return '';
        }
    }

	protected function configureShowFields(ShowMapper $filter)
	{
		$this->configureFields($filter); // TODO: Change the autogenerated stub
	}

	public function createQuery($context = 'list')
	{
		/** @var QueryBuilder $queryBuilder */
		$queryBuilder = parent::createQuery($context);

		$rootAliases = $queryBuilder->getRootAliases();
		$rootAlias = current($rootAliases);

		if(!$this->isGranted('ROLE_SUPER_ADMIN')){
			$queryBuilder->andWhere($queryBuilder->getRootAlias() . '.supplier = :user_supplier');
			$actualSupplier = $this->getActualSupplier();
			$queryBuilder->setParameter('user_supplier', $actualSupplier);
		}

		if (!$this->isGranted('ROLE_SUPPLIER') && !$this->isGranted('ROLE_SUPPLIER_ADMIN')) {
			$queryBuilder->andWhere($queryBuilder->getRootAlias() . '.user = :user');
			$queryBuilder->setParameter('user', $this->getSubjectUser());
		}

		return $queryBuilder;
	}

	private function getActualSupplier(){
		$activeSupplier = $this->getSubjectUser()->getClientsActiveSupplier();
		$supplier = $this->getConfigurationPool()->getContainer()->get('security.context')->getToken()->getUser()->getSupplier();
		$actualSupplier = null;
		if(($this->isGranted('ROLE_SUPPLIER') || $this->isGranted('ROLE_SUPPLIER_ADMIN')) && $supplier){
			$actualSupplier = $supplier;
		}elseif($this->isGranted('ROLE_CLIENT') && $activeSupplier){
			$actualSupplier = $activeSupplier;
		}

		return $actualSupplier;
	}

	public function getSubjectUser()
	{
		if ($this->getSubject()) {
			return $this->getSubject()->getUser();
		}

		if ($this->isChild() && $parentAdmin = $this->getParent()) {
			$parentSubject = $parentAdmin->getSubject();
			if ($parentSubject instanceof User) {
				return $parentSubject;
			}
		}
		return $this->securityContext->getToken()->getUser();

	}
}