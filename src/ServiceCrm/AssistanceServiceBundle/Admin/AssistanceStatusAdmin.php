<?php
/**
 * Created by PhpStorm.
 * User: krTeam
 * Date: 26/04/14
 * Time: 00:09
 */

namespace ServiceCrm\AssistanceServiceBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class AssistanceStatusAdmin extends Admin
{

    public function configureRoutes(RouteCollection $collection){
        //$collection->clearExcept(array('create','list'));
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $sort = array();
        for($i=1;$i<=20;$i++){
            $sort[$i] = $i;
        }

        $formMapper
            ->add('name', 'text', array('label' => 'label.name', 'required'=>false))
            ->add('sort', 'choice', array('required'=>false,'label' => 'label.sort','choices'=>$sort))
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
        ;
    }

} 