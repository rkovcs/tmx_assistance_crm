<?php


namespace ServiceCrm\AssistanceServiceBundle\Block;


use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\UserBundle\Block\AccountBlockService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

class AssistanceListerBlockService extends BaseBlockService
{
	/** @var  ContainerInterface */
	protected $container;

	public function execute(BlockContextInterface $blockContext, Response $response = null)
	{

		$assistanceRepository = $this->container->get('doctrine.orm.default_entity_manager')->getRepository('ServiceCrmAssistanceServiceBundle:Assistance');
		$securityContext = $this->container->get('security.context');

		$assistances = $assistanceRepository->findAll();

		return $this->renderResponse('@ServiceCrmAssistanceService/Block/assistance_lister.html.twig', array(
			'block_context'  => $blockContext,
			'block'          => $blockContext->getBlock(),
			'assistances'    => $assistances,
		), $response);
	}

	/**
	 * @param FormMapper $form
	 * @param BlockInterface $block
	 *
	 * @return void
	 */
	public function buildEditForm(FormMapper $form, BlockInterface $block)
	{
		// TODO: Implement buildEditForm() method.
	}

	/**
	 * @param ErrorElement $errorElement
	 * @param BlockInterface $block
	 *
	 * @return void
	 */
	public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
	{
		// TODO: Implement validateBlock() method.
	}

	/**
	 * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
	 */
	public function setContainer($container)
	{
		$this->container = $container;
	}
}