<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 26/06/14
 * Time: 21:35
 */

namespace ServiceCrm\AssistanceServiceBundle\Constraints;

use Symfony\Component\Validator\Constraint;

class AssistanceRequiringDate extends Constraint {

    public $message = 'Az átvétel dátuma, nem eshet hétvégére. Illetve nagyobbnak kell lennie mint a mai nap +2 nap!';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }

} 