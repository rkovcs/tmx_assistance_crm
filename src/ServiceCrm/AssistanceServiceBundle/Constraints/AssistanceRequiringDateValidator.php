<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 25/06/14
 * Time: 21:57
 */

namespace ServiceCrm\AssistanceServiceBundle\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class AssistanceRequiringDateValidator extends ConstraintValidator {

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     *
     * @api
     */
    public function validate($value, Constraint $constraint)
    {
        $dw = date( "w", $value->getTimestamp());

        if(
        ($dw == 0 || $dw == 6) ||
        ($value->format('Y-m-d') == date('Y-m-d')) ||
        (time()+(86400*2) >= $value->getTimestamp())
        ){
            $this->context->addViolation($constraint->message, array());
        }
    }
}