<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 13/05/14
 * Time: 21:38
 */

namespace ServiceCrm\AssistanceServiceBundle\Controller\CRUD;

use Sonata\AdminBundle\Controller\CRUDController as CRUDController;


class RequiringCourierCRUDController extends CRUDController {


    public function createAction()
    {
        $request = $this->container->get('request');
        $supplier = $request->get('supplier', null);
        if ('GET' === $request->getMethod()) {
            if (!$supplier && ($this->get('security.context')->isGranted('ROLE_SUPER_ADMIN') || $this->get('security.context')->isGranted('ROLE_SUPPLIER_ADMIN'))) {
                $clientsSuppliers = $this->admin->getSubjectUser()->getClientsSuppliers();
                return $this->render('@ServiceCrmProduct/ProductAdmin/create_supplierselect.html.twig', array(
                    'suppliers' => $clientsSuppliers,
                    'admin' => $this->admin,
                    'admin_pool' => $this->container->get('sonata.admin.pool'),
                    'action' => 'create',
                    'object' => $this->admin->getNewInstance(),
                ));
            }
        } else {
            $this->admin->getNewInstance();
        }
        return parent::createAction();
    }

    public function listAction(){
        return parent::listAction();
    }

} 