<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 14/05/14
 * Time: 13:19
 */

namespace ServiceCrm\AssistanceServiceBundle\Entity;

use ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierBase AS BaseClass;
use Doctrine\ORM\Mapping as ORM;

/**
 * RequiringCourier
 * @ORM\Entity(repositoryClass="ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierRepository")
 */
class RequiringCourierUser extends BaseClass {

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

}
