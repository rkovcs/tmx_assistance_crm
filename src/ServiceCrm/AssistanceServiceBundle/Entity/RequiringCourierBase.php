<?php

namespace ServiceCrm\AssistanceServiceBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * RequiringCourier
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\Table(name="requiring_courier")
 * @ORM\Entity(repositoryClass="ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierRepository")
 * @ORM\DiscriminatorColumn(name="user_type", type="string")
 * @ORM\DiscriminatorMap({
 *  "admin" =   "ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierAdmin",
 *  "user" =   "ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierUser",
 * })
 * @Gedmo\Loggable
 */
class RequiringCourierBase
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="alias_uniqid", type="integer")
     */
    private $aliasUniqid;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * @ORM\Column(name="updated", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updated;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="ServiceCrm\ProductBundle\Entity\Product")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     * })
     *
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="accessories", type="string", length=255, nullable=true)
     */
    private $accessories;

    /**
     * @var string
     *
     * @ORM\Column(name="error_description", type="text")
     */
    private $errorDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="replacement_unit", type="boolean", nullable=true)
     */
    private $replacementUnit;

    /**
     * @var string
     *
     * @ORM\Column(name="admin_comment", type="text", nullable=true)
     * @Gedmo\Versioned
     */
    protected $adminComment;


    /**
     * @var string
     *
     * @ORM\Column(name="return_address", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $returnAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="return_address_zip", type="string", nullable=true)
     * @Gedmo\Versioned
     */
    protected $returnAddressZip;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="return_date", type="datetime", nullable=true)
     * @Gedmo\Versioned
     */
    protected $returnDate;


    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     *
     */
    private $user;

	/**
	 * @var integer
	 *
	 * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="create_user_id", referencedColumnName="id")
	 * })
	 *
	 */
	private $createUser;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=16)
     * @Gedmo\Versioned
     */
    private $zip;


    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     * @Gedmo\Versioned
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     * @Gedmo\Versioned
     */
    private $address;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     * @Gedmo\Versioned
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=64)
     * @Gedmo\Versioned
     */
    private $phone;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="ServiceCrm\AssistanceServiceBundle\Entity\Assistance")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assistance_id", referencedColumnName="id")
     * })
     * @Gedmo\Versioned
     *
     */
    private $assistance;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="ServiceCrm\AssistanceServiceBundle\Entity\AssistanceStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assistance_status_id", referencedColumnName="id")
     * })
     * @Gedmo\Versioned
     *
     */
    private $assistanceStatus;

	/**
	 * @var ArrayCollection
	 * @ORM\ManyToMany(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
	 * @ORM\JoinTable(name="requiring_courier_media",
	 *      joinColumns={@ORM\JoinColumn(name="assistance_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="media_id", referencedColumnName="id")}
     * )
	 */
	private $media;


    /**
     * @var Supplier
     *
     * @ORM\ManyToOne(targetEntity="ServiceCrm\ProductBundle\Entity\Supplier")
     */
    private $supplier;

	function __construct()
	{
		$this->media = new ArrayCollection();
	}

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accessories
     *
     * @param string $accessories
     * @return \ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierBase
     */
    public function setAccessories($accessories)
    {
        $this->accessories = $accessories;

        return $this;
    }

    /**
     * Get accessories
     *
     * @return string 
     */
    public function getAccessories()
    {
        return $this->accessories;
    }

    /**
     * Set errorDescription
     *
     * @param string $errorDescription
     * @return \ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierBase
     */
    public function setErrorDescription($errorDescription)
    {
        $this->errorDescription = $errorDescription;

        return $this;
    }

    /**
     * Get errorDescription
     *
     * @return string 
     */
    public function getErrorDescription()
    {
        return $this->errorDescription;
    }

    /**
     * Set replacementUnit
     *
     * @param boolean $replacementUnit
     * @return \ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierBase
     */
    public function setReplacementUnit($replacementUnit)
    {
        $this->replacementUnit = $replacementUnit;

        return $this;
    }

    /**
     * Get replacementUnit
     *
     * @return boolean 
     */
    public function getReplacementUnit()
    {
        return $this->replacementUnit;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return \ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierBase
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return \ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierBase
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return \ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierBase
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return \ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierBase
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set product
     *
     * @param \ServiceCrm\ProductBundle\Entity\Product $product
     * @return \ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierBase
     */
    public function setProduct(\ServiceCrm\ProductBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \ServiceCrm\ProductBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return \ServiceCrm\AssistanceServiceBundle\Entity\RequiringCourierBase
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param mixed $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Set assistance
     *
     * @param \ServiceCrm\AssistanceServiceBundle\Entity\Assistance $assistance
     * @return RequiringCourierBase
     */
    public function setAssistance(\ServiceCrm\AssistanceServiceBundle\Entity\Assistance $assistance = null)
    {
        $this->assistance = $assistance;
        return $this;
    }

    /**
     * Get assistance
     *
     * @return \ServiceCrm\AssistanceServiceBundle\Entity\Assistance 
     */
    public function getAssistance()
    {
        return $this->assistance;
    }

    /**
     * Set assistance
     *
     * @param \ServiceCrm\AssistanceServiceBundle\Entity\Assistance $assistance
     * @return RequiringCourierBase
     */
    public function setAssistanceStatus(\ServiceCrm\AssistanceServiceBundle\Entity\AssistanceStatus $assistanceStatus = null)
    {
        $this->assistanceStatus = $assistanceStatus;

        return $this;
    }

    /**
     * Get assistance
     *
     * @return \ServiceCrm\AssistanceServiceBundle\Entity\Assistance
     */
    public function getAssistanceStatus()
    {
        return $this->assistanceStatus;
    }

    public function __toString(){

       if($this->getProduct() instanceof \ServiceCrm\ProductBundle\Entity\Product ){
            return $this->getProduct()->getType();
       }else{
           return '';
       }
    }

	/**
	 * @return int
	 */
	public function getCreateUser()
	{
		return $this->createUser;
	}

	/**
	 * @param int $createUser
	 */
	public function setCreateUser($createUser)
	{
		$this->createUser = $createUser;
	}

    /**
     * @return string
     */
    public function getAdminComment()
    {
        return $this->adminComment;
    }

    /**
     * @param string $adminComment
     */
    public function setAdminComment($adminComment)
    {
        $this->adminComment = $adminComment;
    }

    /**
     * @return string
     */
    public function getReturnAddress()
    {
        return $this->returnAddress;
    }

    /**
     * @param string $returnAddress
     */
    public function setReturnAddress($returnAddress)
    {
        $this->returnAddress = $returnAddress;
    }

    /**
     * @return \DateTime
     */
    public function getReturnDate()
    {
        return $this->returnDate;
    }

    /**
     * @param \DateTime $returnDate
     */
    public function setReturnDate($returnDate)
    {
        $this->returnDate = $returnDate;
    }



    /**
     * Set returnAddressZip
     *
     * @param string $returnAddressZip
     * @return RequiringCourierAdmin
     */
    public function setReturnAddressZip($returnAddressZip)
    {
        $this->returnAddressZip = $returnAddressZip;

        return $this;
    }

    /**
     * Get returnAddressZip
     *
     * @return string
     */
    public function getReturnAddressZip()
    {
        return $this->returnAddressZip;
    }

	/**
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getMedia()
	{
		return $this->media;
	}

	/**
	 * @param \Doctrine\Common\Collections\ArrayCollection $media
	 */
	public function setMedia($media)
	{
		$this->media = $media;
	}

    /**
     * Set supplier
     *
     * @param \ServiceCrm\ProductBundle\Entity\Supplier $supplier
     * @return RequiringCourierBase
     */
    public function setSupplier(\ServiceCrm\ProductBundle\Entity\Supplier $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \ServiceCrm\ProductBundle\Entity\Supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return int
     */
    public function getAliasUniqid()
    {
        return $this->aliasUniqid;
    }

    /**
     * @param int $aliasUniqid
     */
    public function setAliasUniqid($aliasUniqid)
    {
        $this->aliasUniqid = $aliasUniqid;
    }

    /**
     * @return int
     */
    public function getPrefixAliasUniqid()
    {
        return 'F-'.$this->aliasUniqid;
    }


}
