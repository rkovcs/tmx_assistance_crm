<?php

namespace ServiceCrm\AssistanceServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Assistance
 * @ORM\Table(name="assistance")
 * @ORM\Entity()
 */
class Assistance
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="system_name", type="string", length=64, nullable=false)
     */
    private $systemName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="ServiceCrm\AssistanceServiceBundle\Entity\AssistanceStatus", inversedBy="assistance", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="assistance_assistance_status",
     *      joinColumns={@ORM\JoinColumn(name="assistance_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="assistance_status_id", referencedColumnName="id")}
     *      )
     */
    private $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Assistance
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set systemName
     *
     * @param string $systemName
     * @return Assistance
     */
    public function setSystemName($systemName)
    {
        $this->systemName = $systemName;

        return $this;
    }

    /**
     * Get systemName
     *
     * @return string 
     */
    public function getSystemName()
    {
        return $this->systemName;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Assistance
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }


    public function __toString()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->status = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add status
     *
     * @param \ServiceCrm\AssistanceServiceBundle\Entity\AssistanceStatus $status
     * @return \ServiceCrm\AssistanceServiceBundle\Entity\Assistance
     */
    public function addStatus(\ServiceCrm\AssistanceServiceBundle\Entity\AssistanceStatus $status)
    {
        $this->status[] = $status;

        return $this;
    }

    /**
     * Remove status
     *
     * @param \ServiceCrm\AssistanceServiceBundle\Entity\AssistanceStatus $status
     */
    public function removeStatus(\ServiceCrm\AssistanceServiceBundle\Entity\AssistanceStatus $status)
    {
        $this->status->removeElement($status);
    }

    /**
     * Get status
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStatus()
    {
        return $this->status;
    }
}
