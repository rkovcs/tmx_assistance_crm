<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 01/06/14
 * Time: 12:46
 */

namespace ServiceCrm\AssistanceServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * AssistanceStatus
 *
 * @ORM\Table(name="assistance_status")
 * @ORM\Entity(repositoryClass="ServiceCrm\AssistanceServiceBundle\Entity\AssistanceStatusRepository")
 */
class AssistanceStatus {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=128, nullable=false)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var
     * @ORM\Column(name="sort", type="integer", nullable=true)
     */
    private $sort;

    /**
     * @var
     *
     * @ORM\OneToMany(targetEntity="ServiceCrm\AssistanceServiceBundle\Entity\Assistance", mappedBy="status", cascade={"all"})
     */
    private $assistance;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AssistanceStatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return AssistanceStatus
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function __toString(){
        return $this->name;
    }


    /**
     * Set sort
     *
     * @param integer $sort
     * @return AssistanceStatus
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return integer 
     */
    public function getSort()
    {
        return $this->sort;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->assistance = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add assistance
     *
     * @param \ServiceCrm\ProductBundle\Entity\SupplierManufacturer $assistance
     * @return AssistanceStatus
     */
    public function addAssistance(\ServiceCrm\ProductBundle\Entity\SupplierManufacturer $assistance)
    {
        $this->assistance[] = $assistance;

        return $this;
    }

    /**
     * Remove assistance
     *
     * @param \ServiceCrm\AssistanceServiceBundle\Entity\Assistance $assistance
     */
    public function removeAssistance(\ServiceCrm\AssistanceServiceBundle\Entity\Assistance $assistance)
    {
        $this->assistance->removeElement($assistance);
    }

    /**
     * Get assistance
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAssistance()
    {
        return $this->assistance;
    }
}
