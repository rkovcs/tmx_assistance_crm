<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 25/06/14
 * Time: 21:57
 */

namespace ServiceCrm\ProfileBundle\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class PhoneValidator extends ConstraintValidator {

    /**
     * Checks if the passed value is valid.
     *
     * @param mixed $value The value that should be validated
     * @param Constraint $constraint The constraint for the validation
     *
     * @api
     */
    public function validate($value, Constraint $constraint)
    {
        if(!preg_match("/^[0-9]{2}-[0-9]{3}-[0-9]{4}$/", $value)) {
            $this->context->addViolation($constraint->message, array('%string%' => $value));
        }
    }
}