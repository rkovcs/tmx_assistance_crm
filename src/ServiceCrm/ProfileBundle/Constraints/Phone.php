<?php
/**
 * Created by PhpStorm.
 * User: kovacsrobert
 * Date: 26/06/14
 * Time: 21:35
 */

namespace ServiceCrm\ProfileBundle\Constraints;

use Symfony\Component\Validator\Constraint;

class Phone extends Constraint {

    public $message = 'A telefonszám formátuma nem megfelelő!';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }

} 