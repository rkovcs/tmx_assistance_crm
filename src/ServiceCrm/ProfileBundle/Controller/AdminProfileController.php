<?php

namespace ServiceCrm\ProfileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class AdminProfileController extends Controller
{
    public function indexAction()
    {
        $user = $this->container->get('security.context')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $form = $this->container->get('sonata.user.profile.form');
        $formHandler = $this->container->get('sonata.user.profile.form.handler');
        $process = $formHandler->process($user);
        if ($process) {
            $this->setFlash('sonata_user_success', 'profile.flash.updated');
        }
        $adminPool = $this->container->get('sonata.admin.pool');
        return $this->render('SonataAdminBundle:Profile:admin_profile.html.twig', array(
            'form'            => $form->createView(),
            'base_template'   => $this->getBaseTemplate($adminPool),
            'admin_pool'      => $adminPool,
        ));
    }

    /**
     * @param string $action
     * @param string $value
     */
    protected function setFlash($action, $value)
    {
        $this->container->get('session')->getFlashBag()->set($action, $value);
    }

    /**
     * @return string
     */
    protected function getBaseTemplate($adminPool)
    {
        if ($this->getRequest()->isXmlHttpRequest()) {
            return $adminPool->getTemplate('ajax');
        }

        return $adminPool->getTemplate('layout');
    }
}
