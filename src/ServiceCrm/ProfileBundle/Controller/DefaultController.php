<?php

namespace ServiceCrm\ProfileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function profileAction($name)
    {
        return $this->render('SonataAdminBundle:Profile:index.html.twig', array('name' => $name));
    }
}
