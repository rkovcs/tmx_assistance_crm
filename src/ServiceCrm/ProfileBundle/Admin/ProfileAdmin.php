<?php
/**
 * Created by PhpStorm.
 * User: krTeam
 * Date: 26/04/14
 * Time: 00:09
 */

namespace ServiceCrm\ProfileBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use \Symfony\Component\Security\Core\SecurityContextInterface;

class ProfileAdmin extends Admin
{

    protected function configureRoutes(RouteCollection $collection)
    {
        //$collection->remove('list');
        //$collection->remove('create');
        //$collection->add('edit');
    }


    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        // TODO: Nincs használva !!
        $formMapper
            ->add('username', 'text', array('label' => 'form.label.username','attr'=>array('readonly'=>true)))
            ->add('email', 'text', array('label' => 'form.label.username','attr'=>array('readonly'=>true)))
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('username')
            ->add('email')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('username')
            ->add('email')
        ;
    }

} 