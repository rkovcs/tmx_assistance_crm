-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Hoszt: localhost
-- Létrehozás ideje: 2014. aug. 22. 16:54
-- Szerver verzió: 5.5.25
-- PHP verzió: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Adatbázis: `tmx`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet: `acl_classes`
--

CREATE TABLE `acl_classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_69DD750638A36066` (`class_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

--
-- A tábla adatainak kiíratása `acl_classes`
--

INSERT INTO `acl_classes` (`id`, `class_type`) VALUES
(18, 'Application\\Sonata\\MediaBundle\\Entity\\Media'),
(5, 'Application\\Sonata\\UserBundle\\Admin\\Model\\GroupAdmin'),
(4, 'Application\\Sonata\\UserBundle\\Admin\\Model\\UserAdmin'),
(15, 'Application\\Sonata\\UserBundle\\Entity\\User'),
(14, 'ServiceCrm\\AssistanceServiceBundle\\Admin\\AssistanceAdmin'),
(13, 'ServiceCrm\\AssistanceServiceBundle\\Admin\\AssistanceStatusAdmin'),
(11, 'ServiceCrm\\AssistanceServiceBundle\\Admin\\RequiringCourierAdmin'),
(12, 'ServiceCrm\\AssistanceServiceBundle\\Admin\\RequiringCourierAdministratorAdmin'),
(17, 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser'),
(8, 'ServiceCrm\\ProductBundle\\Admin\\ManufacturerAdmin'),
(6, 'ServiceCrm\\ProductBundle\\Admin\\ProductAdmin'),
(7, 'ServiceCrm\\ProductBundle\\Admin\\ProductTypeAdmin'),
(9, 'ServiceCrm\\ProductBundle\\Admin\\SupplierAdmin'),
(10, 'ServiceCrm\\ProductBundle\\Admin\\SupplierManufacturerAdmin'),
(16, 'ServiceCrm\\ProductBundle\\Entity\\Product'),
(19, 'ServiceCrm\\ProductBundle\\Entity\\ProductType'),
(1, 'Sonata\\MediaBundle\\Admin\\GalleryAdmin'),
(3, 'Sonata\\MediaBundle\\Admin\\GalleryHasMediaAdmin'),
(2, 'Sonata\\MediaBundle\\Admin\\ORM\\MediaAdmin');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `acl_entries`
--

CREATE TABLE `acl_entries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL,
  `object_identity_id` int(10) unsigned DEFAULT NULL,
  `security_identity_id` int(10) unsigned NOT NULL,
  `field_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ace_order` smallint(5) unsigned NOT NULL,
  `mask` int(11) NOT NULL,
  `granting` tinyint(1) NOT NULL,
  `granting_strategy` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `audit_success` tinyint(1) NOT NULL,
  `audit_failure` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_46C8B806EA000B103D9AB4A64DEF17BCE4289BF4` (`class_id`,`object_identity_id`,`field_name`,`ace_order`),
  KEY `IDX_46C8B806EA000B103D9AB4A6DF9183C9` (`class_id`,`object_identity_id`,`security_identity_id`),
  KEY `IDX_46C8B806EA000B10` (`class_id`),
  KEY `IDX_46C8B8063D9AB4A6` (`object_identity_id`),
  KEY `IDX_46C8B806DF9183C9` (`security_identity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=131 ;

--
-- A tábla adatainak kiíratása `acl_entries`
--

INSERT INTO `acl_entries` (`id`, `class_id`, `object_identity_id`, `security_identity_id`, `field_name`, `ace_order`, `mask`, `granting`, `granting_strategy`, `audit_success`, `audit_failure`) VALUES
(1, 1, NULL, 1, NULL, 0, 64, 1, 'all', 0, 0),
(2, 1, NULL, 2, NULL, 1, 8224, 1, 'all', 0, 0),
(3, 1, NULL, 3, NULL, 2, 4098, 1, 'all', 0, 0),
(4, 1, NULL, 4, NULL, 3, 4096, 1, 'all', 0, 0),
(5, 2, NULL, 5, NULL, 0, 64, 1, 'all', 0, 0),
(6, 2, NULL, 6, NULL, 1, 8224, 1, 'all', 0, 0),
(7, 2, NULL, 7, NULL, 2, 4098, 1, 'all', 0, 0),
(8, 2, NULL, 8, NULL, 3, 4096, 1, 'all', 0, 0),
(9, 3, NULL, 9, NULL, 0, 64, 1, 'all', 0, 0),
(10, 3, NULL, 10, NULL, 1, 8224, 1, 'all', 0, 0),
(11, 3, NULL, 11, NULL, 2, 4098, 1, 'all', 0, 0),
(12, 3, NULL, 12, NULL, 3, 4096, 1, 'all', 0, 0),
(13, 4, NULL, 13, NULL, 0, 64, 1, 'all', 0, 0),
(14, 4, NULL, 14, NULL, 1, 8224, 1, 'all', 0, 0),
(15, 4, NULL, 15, NULL, 2, 4098, 1, 'all', 0, 0),
(16, 4, NULL, 16, NULL, 3, 4096, 1, 'all', 0, 0),
(17, 5, NULL, 17, NULL, 0, 64, 1, 'all', 0, 0),
(18, 5, NULL, 18, NULL, 1, 8224, 1, 'all', 0, 0),
(19, 5, NULL, 19, NULL, 2, 4098, 1, 'all', 0, 0),
(20, 5, NULL, 20, NULL, 3, 4096, 1, 'all', 0, 0),
(21, 6, NULL, 21, NULL, 0, 64, 1, 'all', 0, 0),
(22, 6, NULL, 22, NULL, 1, 8224, 1, 'all', 0, 0),
(23, 6, NULL, 23, NULL, 2, 4098, 1, 'all', 0, 0),
(24, 6, NULL, 24, NULL, 3, 4096, 1, 'all', 0, 0),
(25, 7, NULL, 25, NULL, 0, 64, 1, 'all', 0, 0),
(26, 7, NULL, 26, NULL, 1, 8224, 1, 'all', 0, 0),
(27, 7, NULL, 27, NULL, 2, 4098, 1, 'all', 0, 0),
(28, 7, NULL, 28, NULL, 3, 4096, 1, 'all', 0, 0),
(29, 8, NULL, 29, NULL, 0, 64, 1, 'all', 0, 0),
(30, 8, NULL, 30, NULL, 1, 8224, 1, 'all', 0, 0),
(31, 8, NULL, 31, NULL, 2, 4098, 1, 'all', 0, 0),
(32, 8, NULL, 32, NULL, 3, 4096, 1, 'all', 0, 0),
(33, 9, NULL, 33, NULL, 0, 64, 1, 'all', 0, 0),
(34, 9, NULL, 34, NULL, 1, 8224, 1, 'all', 0, 0),
(35, 9, NULL, 35, NULL, 2, 4098, 1, 'all', 0, 0),
(36, 9, NULL, 36, NULL, 3, 4096, 1, 'all', 0, 0),
(37, 10, NULL, 37, NULL, 0, 64, 1, 'all', 0, 0),
(38, 10, NULL, 38, NULL, 1, 8224, 1, 'all', 0, 0),
(39, 10, NULL, 39, NULL, 2, 4098, 1, 'all', 0, 0),
(40, 10, NULL, 40, NULL, 3, 4096, 1, 'all', 0, 0),
(41, 11, NULL, 41, NULL, 0, 64, 1, 'all', 0, 0),
(42, 11, NULL, 42, NULL, 1, 8224, 1, 'all', 0, 0),
(43, 11, NULL, 43, NULL, 2, 4098, 1, 'all', 0, 0),
(44, 11, NULL, 44, NULL, 3, 4096, 1, 'all', 0, 0),
(45, 12, NULL, 45, NULL, 0, 64, 1, 'all', 0, 0),
(46, 12, NULL, 46, NULL, 1, 8224, 1, 'all', 0, 0),
(47, 12, NULL, 47, NULL, 2, 4098, 1, 'all', 0, 0),
(48, 12, NULL, 48, NULL, 3, 4096, 1, 'all', 0, 0),
(49, 13, NULL, 49, NULL, 0, 64, 1, 'all', 0, 0),
(50, 13, NULL, 50, NULL, 1, 8224, 1, 'all', 0, 0),
(51, 13, NULL, 51, NULL, 2, 4098, 1, 'all', 0, 0),
(52, 13, NULL, 52, NULL, 3, 4096, 1, 'all', 0, 0),
(53, 14, NULL, 53, NULL, 0, 64, 1, 'all', 0, 0),
(54, 14, NULL, 54, NULL, 1, 8224, 1, 'all', 0, 0),
(55, 14, NULL, 55, NULL, 2, 4098, 1, 'all', 0, 0),
(56, 14, NULL, 56, NULL, 3, 4096, 1, 'all', 0, 0),
(57, 15, NULL, 13, NULL, 0, 64, 1, 'all', 0, 0),
(58, 15, NULL, 14, NULL, 1, 32, 1, 'all', 0, 0),
(59, 15, NULL, 15, NULL, 2, 4, 1, 'all', 0, 0),
(60, 15, NULL, 16, NULL, 3, 1, 1, 'all', 0, 0),
(61, 15, 15, 57, NULL, 0, 128, 1, 'all', 0, 0),
(62, 15, 16, 57, NULL, 0, 128, 1, 'all', 0, 0),
(63, 16, NULL, 21, NULL, 0, 64, 1, 'all', 0, 0),
(64, 16, NULL, 22, NULL, 1, 32, 1, 'all', 0, 0),
(65, 16, NULL, 23, NULL, 2, 4, 1, 'all', 0, 0),
(66, 16, NULL, 24, NULL, 3, 1, 1, 'all', 0, 0),
(67, 16, 17, 58, NULL, 0, 128, 1, 'all', 0, 0),
(68, 17, NULL, 41, NULL, 0, 64, 1, 'all', 0, 0),
(69, 17, NULL, 42, NULL, 1, 32, 1, 'all', 0, 0),
(70, 17, NULL, 43, NULL, 2, 4, 1, 'all', 0, 0),
(71, 17, NULL, 44, NULL, 3, 1, 1, 'all', 0, 0),
(72, 17, 18, 58, NULL, 0, 128, 1, 'all', 0, 0),
(73, 17, 19, 59, NULL, 0, 128, 1, 'all', 0, 0),
(74, 17, 20, 59, NULL, 0, 128, 1, 'all', 0, 0),
(75, 15, 21, 60, NULL, 0, 128, 1, 'all', 0, 0),
(76, 16, 22, 57, NULL, 0, 128, 1, 'all', 0, 0),
(77, 17, 23, 57, NULL, 0, 128, 1, 'all', 0, 0),
(78, 17, 24, 60, NULL, 0, 128, 1, 'all', 0, 0),
(79, 16, 25, 61, NULL, 0, 128, 1, 'all', 0, 0),
(80, 17, 26, 61, NULL, 0, 128, 1, 'all', 0, 0),
(81, 18, NULL, 5, NULL, 0, 64, 1, 'all', 0, 0),
(82, 18, NULL, 6, NULL, 1, 32, 1, 'all', 0, 0),
(83, 18, NULL, 7, NULL, 2, 4, 1, 'all', 0, 0),
(84, 18, NULL, 8, NULL, 3, 1, 1, 'all', 0, 0),
(85, 18, 27, 57, NULL, 0, 128, 1, 'all', 0, 0),
(86, 18, 28, 57, NULL, 0, 128, 1, 'all', 0, 0),
(87, 16, 29, 58, NULL, 0, 128, 1, 'all', 0, 0),
(88, 16, 30, 58, NULL, 0, 128, 1, 'all', 0, 0),
(89, 16, 31, 61, NULL, 0, 128, 1, 'all', 0, 0),
(90, 17, 32, 61, NULL, 0, 128, 1, 'all', 0, 0),
(91, 19, NULL, 25, NULL, 0, 64, 1, 'all', 0, 0),
(92, 19, NULL, 26, NULL, 1, 32, 1, 'all', 0, 0),
(93, 19, NULL, 27, NULL, 2, 4, 1, 'all', 0, 0),
(94, 19, NULL, 28, NULL, 3, 1, 1, 'all', 0, 0),
(95, 19, 33, 57, NULL, 0, 128, 1, 'all', 0, 0),
(96, 16, 34, 58, NULL, 0, 128, 1, 'all', 0, 0),
(97, 15, 35, 59, NULL, 0, 128, 1, 'all', 0, 0),
(98, 16, 36, 59, NULL, 0, 128, 1, 'all', 0, 0),
(99, 16, 37, 58, NULL, 0, 128, 1, 'all', 0, 0),
(100, 16, 38, 58, NULL, 0, 128, 1, 'all', 0, 0),
(101, 16, 39, 61, NULL, 0, 128, 1, 'all', 0, 0),
(102, 16, 40, 62, NULL, 0, 128, 1, 'all', 0, 0),
(103, 17, 41, 62, NULL, 0, 128, 1, 'all', 0, 0),
(104, 17, 42, 58, NULL, 0, 128, 1, 'all', 0, 0),
(105, 16, 43, 63, NULL, 0, 128, 1, 'all', 0, 0),
(106, 17, 44, 63, NULL, 0, 128, 1, 'all', 0, 0),
(107, 16, 45, 62, NULL, 0, 128, 1, 'all', 0, 0),
(108, 17, 46, 62, NULL, 0, 128, 1, 'all', 0, 0),
(109, 16, 47, 61, NULL, 0, 128, 1, 'all', 0, 0),
(110, 16, 48, 64, NULL, 0, 128, 1, 'all', 0, 0),
(111, 16, 49, 63, NULL, 0, 128, 1, 'all', 0, 0),
(113, 17, 51, 63, NULL, 0, 128, 1, 'all', 0, 0),
(114, 16, 52, 61, NULL, 0, 128, 1, 'all', 0, 0),
(115, 17, 53, 61, NULL, 0, 128, 1, 'all', 0, 0),
(116, 16, 54, 65, NULL, 0, 128, 1, 'all', 0, 0),
(117, 17, 55, 65, NULL, 0, 128, 1, 'all', 0, 0),
(118, 17, 56, 61, NULL, 0, 128, 1, 'all', 0, 0),
(119, 17, 57, 61, NULL, 0, 128, 1, 'all', 0, 0),
(120, 17, 58, 61, NULL, 0, 128, 1, 'all', 0, 0),
(121, 17, 59, 65, NULL, 0, 128, 1, 'all', 0, 0),
(122, 17, 60, 61, NULL, 0, 128, 1, 'all', 0, 0),
(123, 16, 61, 63, NULL, 0, 128, 1, 'all', 0, 0),
(124, 17, 62, 63, NULL, 0, 128, 1, 'all', 0, 0),
(125, 16, 63, 63, NULL, 0, 128, 1, 'all', 0, 0),
(126, 17, 64, 63, NULL, 0, 128, 1, 'all', 0, 0),
(127, 16, 65, 66, NULL, 0, 128, 1, 'all', 0, 0),
(128, 17, 66, 66, NULL, 0, 128, 1, 'all', 0, 0),
(129, 15, 67, 57, NULL, 0, 128, 1, 'all', 0, 0),
(130, 16, 68, 57, NULL, 0, 128, 1, 'all', 0, 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `acl_object_identities`
--

CREATE TABLE `acl_object_identities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_object_identity_id` int(10) unsigned DEFAULT NULL,
  `class_id` int(10) unsigned NOT NULL,
  `object_identifier` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `entries_inheriting` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9407E5494B12AD6EA000B10` (`object_identifier`,`class_id`),
  KEY `IDX_9407E54977FA751A` (`parent_object_identity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=69 ;

--
-- A tábla adatainak kiíratása `acl_object_identities`
--

INSERT INTO `acl_object_identities` (`id`, `parent_object_identity_id`, `class_id`, `object_identifier`, `entries_inheriting`) VALUES
(1, NULL, 1, 'sonata.media.admin.gallery', 1),
(2, NULL, 2, 'sonata.media.admin.media', 1),
(3, NULL, 3, 'sonata.media.admin.gallery_has_media', 1),
(4, NULL, 4, 'sonata.user.admin.user', 1),
(5, NULL, 5, 'sonata.user.admin.group', 1),
(6, NULL, 6, 'sonata.admin.service_crm.product', 1),
(7, NULL, 7, 'sonata.admin.service_crm.product_type', 1),
(8, NULL, 8, 'sonata.admin.service_crm.manufacturer', 1),
(9, NULL, 9, 'sonata.admin.service_crm.supplier', 1),
(10, NULL, 10, 'sonata.admin.service_crm.supplier_manufacturer', 1),
(11, NULL, 11, 'sonata.admin.service_crm.assistances_requiring_courier', 1),
(12, NULL, 12, 'sonata.admin.service_crm.assistances_requiring_courier_admin', 1),
(13, NULL, 13, 'sonata.admin.service_crm.assistances_assistance_status', 1),
(14, NULL, 14, 'sonata.admin.service_crm.assistance_type', 1),
(15, NULL, 15, '17', 1),
(16, NULL, 15, '18', 1),
(17, NULL, 16, '51', 1),
(18, NULL, 17, '15', 1),
(19, NULL, 17, '16', 1),
(20, NULL, 17, '17', 1),
(21, NULL, 15, '20', 1),
(22, NULL, 16, '52', 1),
(23, NULL, 17, '18', 1),
(24, NULL, 17, '19', 1),
(25, NULL, 16, '53', 1),
(26, NULL, 17, '20', 1),
(27, NULL, 18, '32', 1),
(28, NULL, 18, '33', 1),
(29, NULL, 16, '54', 1),
(30, NULL, 16, '55', 1),
(31, NULL, 16, '56', 1),
(32, NULL, 17, '21', 1),
(33, NULL, 19, '2', 1),
(34, NULL, 16, '57', 1),
(35, NULL, 15, '56', 1),
(36, NULL, 16, '58', 1),
(37, NULL, 16, '59', 1),
(38, NULL, 16, '60', 1),
(39, NULL, 16, '61', 1),
(40, NULL, 16, '62', 1),
(41, NULL, 17, '22', 1),
(42, NULL, 17, '23', 1),
(43, NULL, 16, '63', 1),
(44, NULL, 17, '24', 1),
(45, NULL, 16, '64', 1),
(46, NULL, 17, '25', 1),
(47, NULL, 16, '65', 1),
(48, NULL, 16, '66', 1),
(49, NULL, 16, '67', 1),
(51, NULL, 17, '27', 1),
(52, NULL, 16, '68', 1),
(53, NULL, 17, '28', 1),
(54, NULL, 16, '69', 1),
(55, NULL, 17, '29', 1),
(56, NULL, 17, '30', 1),
(57, NULL, 17, '31', 1),
(58, NULL, 17, '32', 1),
(59, NULL, 17, '33', 1),
(60, NULL, 17, '34', 1),
(61, NULL, 16, '70', 1),
(62, NULL, 17, '35', 1),
(63, NULL, 16, '71', 1),
(64, NULL, 17, '36', 1),
(65, NULL, 16, '72', 1),
(66, NULL, 17, '37', 1),
(67, NULL, 15, '70', 1),
(68, NULL, 16, '73', 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `acl_object_identity_ancestors`
--

CREATE TABLE `acl_object_identity_ancestors` (
  `object_identity_id` int(10) unsigned NOT NULL,
  `ancestor_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`object_identity_id`,`ancestor_id`),
  KEY `IDX_825DE2993D9AB4A6` (`object_identity_id`),
  KEY `IDX_825DE299C671CEA1` (`ancestor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `acl_object_identity_ancestors`
--

INSERT INTO `acl_object_identity_ancestors` (`object_identity_id`, `ancestor_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15),
(16, 16),
(17, 17),
(18, 18),
(19, 19),
(20, 20),
(21, 21),
(22, 22),
(23, 23),
(24, 24),
(25, 25),
(26, 26),
(27, 27),
(28, 28),
(29, 29),
(30, 30),
(31, 31),
(32, 32),
(33, 33),
(34, 34),
(35, 35),
(36, 36),
(37, 37),
(38, 38),
(39, 39),
(40, 40),
(41, 41),
(42, 42),
(43, 43),
(44, 44),
(45, 45),
(46, 46),
(47, 47),
(48, 48),
(49, 49),
(51, 51),
(52, 52),
(53, 53),
(54, 54),
(55, 55),
(56, 56),
(57, 57),
(58, 58),
(59, 59),
(60, 60),
(61, 61),
(62, 62),
(63, 63),
(64, 64),
(65, 65),
(66, 66),
(67, 67),
(68, 68);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `acl_security_identities`
--

CREATE TABLE `acl_security_identities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identifier` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `username` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8835EE78772E836AF85E0677` (`identifier`,`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=67 ;

--
-- A tábla adatainak kiíratása `acl_security_identities`
--

INSERT INTO `acl_security_identities` (`id`, `identifier`, `username`) VALUES
(57, 'Application\\Sonata\\UserBundle\\Entity\\User-admin', 1),
(62, 'Application\\Sonata\\UserBundle\\Entity\\User-Egervarik4', 1),
(64, 'Application\\Sonata\\UserBundle\\Entity\\User-eliasi', 1),
(63, 'Application\\Sonata\\UserBundle\\Entity\\User-polgarzs', 1),
(60, 'Application\\Sonata\\UserBundle\\Entity\\User-provider1', 1),
(66, 'Application\\Sonata\\UserBundle\\Entity\\User-soos.brigitta', 1),
(61, 'Application\\Sonata\\UserBundle\\Entity\\User-szabolcsg', 1),
(65, 'Application\\Sonata\\UserBundle\\Entity\\User-szarkane2011', 1),
(59, 'Application\\Sonata\\UserBundle\\Entity\\User-tmxadmin2', 1),
(58, 'Application\\Sonata\\UserBundle\\Entity\\User-tmxuser1', 1),
(53, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCE_TYPE_ADMIN', 0),
(54, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCE_TYPE_EDITOR', 0),
(56, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCE_TYPE_GUEST', 0),
(55, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCE_TYPE_STAFF', 0),
(49, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_ASSISTANCE_STATUS_ADMIN', 0),
(50, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_ASSISTANCE_STATUS_EDITOR', 0),
(52, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_ASSISTANCE_STATUS_GUEST', 0),
(51, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_ASSISTANCE_STATUS_STAFF', 0),
(41, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_REQUIRING_COURIER_ADMIN', 0),
(45, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_REQUIRING_COURIER_ADMIN_ADMIN', 0),
(46, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_REQUIRING_COURIER_ADMIN_EDITOR', 0),
(48, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_REQUIRING_COURIER_ADMIN_GUEST', 0),
(47, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_REQUIRING_COURIER_ADMIN_STAFF', 0),
(42, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_REQUIRING_COURIER_EDITOR', 0),
(44, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_REQUIRING_COURIER_GUEST', 0),
(43, 'ROLE_SONATA_ADMIN_SERVICE_CRM_ASSISTANCES_REQUIRING_COURIER_STAFF', 0),
(29, 'ROLE_SONATA_ADMIN_SERVICE_CRM_MANUFACTURER_ADMIN', 0),
(30, 'ROLE_SONATA_ADMIN_SERVICE_CRM_MANUFACTURER_EDITOR', 0),
(32, 'ROLE_SONATA_ADMIN_SERVICE_CRM_MANUFACTURER_GUEST', 0),
(31, 'ROLE_SONATA_ADMIN_SERVICE_CRM_MANUFACTURER_STAFF', 0),
(21, 'ROLE_SONATA_ADMIN_SERVICE_CRM_PRODUCT_ADMIN', 0),
(22, 'ROLE_SONATA_ADMIN_SERVICE_CRM_PRODUCT_EDITOR', 0),
(24, 'ROLE_SONATA_ADMIN_SERVICE_CRM_PRODUCT_GUEST', 0),
(23, 'ROLE_SONATA_ADMIN_SERVICE_CRM_PRODUCT_STAFF', 0),
(25, 'ROLE_SONATA_ADMIN_SERVICE_CRM_PRODUCT_TYPE_ADMIN', 0),
(26, 'ROLE_SONATA_ADMIN_SERVICE_CRM_PRODUCT_TYPE_EDITOR', 0),
(28, 'ROLE_SONATA_ADMIN_SERVICE_CRM_PRODUCT_TYPE_GUEST', 0),
(27, 'ROLE_SONATA_ADMIN_SERVICE_CRM_PRODUCT_TYPE_STAFF', 0),
(33, 'ROLE_SONATA_ADMIN_SERVICE_CRM_SUPPLIER_ADMIN', 0),
(34, 'ROLE_SONATA_ADMIN_SERVICE_CRM_SUPPLIER_EDITOR', 0),
(36, 'ROLE_SONATA_ADMIN_SERVICE_CRM_SUPPLIER_GUEST', 0),
(37, 'ROLE_SONATA_ADMIN_SERVICE_CRM_SUPPLIER_MANUFACTURER_ADMIN', 0),
(38, 'ROLE_SONATA_ADMIN_SERVICE_CRM_SUPPLIER_MANUFACTURER_EDITOR', 0),
(40, 'ROLE_SONATA_ADMIN_SERVICE_CRM_SUPPLIER_MANUFACTURER_GUEST', 0),
(39, 'ROLE_SONATA_ADMIN_SERVICE_CRM_SUPPLIER_MANUFACTURER_STAFF', 0),
(35, 'ROLE_SONATA_ADMIN_SERVICE_CRM_SUPPLIER_STAFF', 0),
(1, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_ADMIN', 0),
(2, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_EDITOR', 0),
(4, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_GUEST', 0),
(9, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_HAS_MEDIA_ADMIN', 0),
(10, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_HAS_MEDIA_EDITOR', 0),
(12, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_HAS_MEDIA_GUEST', 0),
(11, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_HAS_MEDIA_STAFF', 0),
(3, 'ROLE_SONATA_MEDIA_ADMIN_GALLERY_STAFF', 0),
(5, 'ROLE_SONATA_MEDIA_ADMIN_MEDIA_ADMIN', 0),
(6, 'ROLE_SONATA_MEDIA_ADMIN_MEDIA_EDITOR', 0),
(8, 'ROLE_SONATA_MEDIA_ADMIN_MEDIA_GUEST', 0),
(7, 'ROLE_SONATA_MEDIA_ADMIN_MEDIA_STAFF', 0),
(17, 'ROLE_SONATA_USER_ADMIN_GROUP_ADMIN', 0),
(18, 'ROLE_SONATA_USER_ADMIN_GROUP_EDITOR', 0),
(20, 'ROLE_SONATA_USER_ADMIN_GROUP_GUEST', 0),
(19, 'ROLE_SONATA_USER_ADMIN_GROUP_STAFF', 0),
(13, 'ROLE_SONATA_USER_ADMIN_USER_ADMIN', 0),
(14, 'ROLE_SONATA_USER_ADMIN_USER_EDITOR', 0),
(16, 'ROLE_SONATA_USER_ADMIN_USER_GUEST', 0),
(15, 'ROLE_SONATA_USER_ADMIN_USER_STAFF', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `assistance`
--

CREATE TABLE `assistance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `system_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- A tábla adatainak kiíratása `assistance`
--

INSERT INTO `assistance` (`id`, `name`, `system_name`, `description`) VALUES
(1, 'Futár igénylés', 'requiring_courier', 'Készülékét az Ön által megadott címről hozzuk el, ezért ennek pontos megadására különös figyelmet szenteljen. A legközelebbi futár felvételi nap a regisztrációt követő 2 munkanap lehet.');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `assistance_assistance_status`
--

CREATE TABLE `assistance_assistance_status` (
  `assistance_id` int(11) NOT NULL,
  `assistance_status_id` int(11) NOT NULL,
  PRIMARY KEY (`assistance_id`,`assistance_status_id`),
  KEY `IDX_B84613BD7096529A` (`assistance_id`),
  KEY `IDX_B84613BD8B74A071` (`assistance_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `assistance_assistance_status`
--

INSERT INTO `assistance_assistance_status` (`assistance_id`, `assistance_status_id`) VALUES
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `assistance_status`
--

CREATE TABLE `assistance_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_62143302989D9B62` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- A tábla adatainak kiíratása `assistance_status`
--

INSERT INTO `assistance_status` (`id`, `name`, `slug`, `sort`) VALUES
(1, 'status1', 'status1', NULL),
(2, 'status2', 'status2', NULL),
(3, 'status4', 'status4', NULL),
(4, 'Státus7', 'status7', NULL),
(5, 'status1', 'status1-1', NULL),
(6, 'status2', 'status2-1', NULL),
(7, 'status1', 'status1-2', NULL),
(8, 'status2', 'status2-2', NULL),
(9, 'Új igény', 'uj-igeny', 1),
(10, 'Evidenciára vár', 'evidenciara-var', 2),
(11, 'Megerősített igény', 'megerositett-igeny', 3),
(12, 'Csúszó átvétel', 'csuszo-atvetel', 4),
(13, 'Készülék átvéve', 'keszulek-atveve', 5),
(14, 'Garanciajegyre vár', 'garanciajegyre-var', 6),
(15, 'Készülék visszadva', 'keszulek-visszadva', 7),
(16, 'Törölt igény', 'torolt-igeny', 8);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `ext_log_entries`
--

CREATE TABLE `ext_log_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `logged_at` datetime NOT NULL,
  `object_id` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` int(11) NOT NULL,
  `data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_class_lookup_idx` (`object_class`),
  KEY `log_date_lookup_idx` (`logged_at`),
  KEY `log_user_lookup_idx` (`username`),
  KEY `log_version_lookup_idx` (`object_id`,`object_class`,`version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=70 ;

--
-- A tábla adatainak kiíratása `ext_log_entries`
--

INSERT INTO `ext_log_entries` (`id`, `action`, `logged_at`, `object_id`, `object_class`, `version`, `data`, `username`) VALUES
(23, 'create', '2014-06-22 16:36:47', '15', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:10:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"8161";s:7:"address";s:3:"osi";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2009-01-01 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:15:"Europe/Budapest";}s:5:"phone";s:9:"234234242";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";N;}', 'tmxuser1'),
(24, 'create', '2014-06-22 16:54:18', '16', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:10:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2342";s:7:"address";s:8:"budapest";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2009-01-01 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:15:"Europe/Budapest";}s:5:"phone";s:10:"2342342342";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";N;}', 'tmxadmin2'),
(25, 'create', '2014-06-22 17:25:41', '17', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:10:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:5:"23423";s:7:"address";s:8:"budapest";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2009-01-01 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:15:"Europe/Budapest";}s:5:"phone";s:8:"23423423";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'tmxadmin2'),
(26, 'update', '2014-06-22 17:36:28', '17', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 2, 'a:3:{s:13:"returnAddress";s:8:"asdfasdf";s:16:"returnAddressZip";s:4:"2342";s:10:"returnDate";O:8:"DateTime":3:{s:4:"date";s:19:"2009-01-01 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:15:"Europe/Budapest";}}', 'tmxadmin2'),
(27, 'update', '2014-06-22 17:37:15', '17', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 3, 'a:1:{s:12:"adminComment";s:6:"sadfas";}', 'tmxadmin2'),
(28, 'update', '2014-06-22 17:37:26', '17', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 4, 'a:1:{s:16:"assistanceStatus";a:1:{s:2:"id";i:14;}}', 'tmxadmin2'),
(29, 'create', '2014-06-22 18:11:19', '18', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:10:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:5:"32423";s:7:"address";s:8:"budapest";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2009-01-01 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:15:"Europe/Budapest";}s:5:"phone";s:11:"06703453453";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'admin'),
(30, 'create', '2014-06-22 18:11:50', '19', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:10:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"3242";s:7:"address";s:8:"budapest";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2009-01-01 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:15:"Europe/Budapest";}s:5:"phone";s:10:"0670345345";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'provider1'),
(31, 'create', '2014-06-23 10:29:07', '20', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:10:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2243";s:7:"address";s:18:"Kóka Ady Endre 21";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-06-01 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}s:5:"phone";s:14:"06-30-280-2985";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'szabolcsg'),
(32, 'update', '2014-06-23 10:37:45', '20', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 2, 'a:5:{s:12:"adminComment";s:22:"Ügyféllel beszéltem";s:13:"returnAddress";s:24:"Kóka, Ady Endre utca 21";s:16:"returnAddressZip";s:4:"2243";s:10:"returnDate";O:8:"DateTime":3:{s:4:"date";s:19:"2009-01-01 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}s:16:"assistanceStatus";a:1:{s:2:"id";i:11;}}', 'tmxadmin2'),
(33, 'update', '2014-06-24 03:45:47', '20', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 3, 'a:1:{s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-06-02 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}}', 'tmxadmin2'),
(34, 'create', '2014-06-24 05:25:28', '21', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:10:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2040";s:7:"address";s:20:"Budaörs Kinizsi 2/B";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-06-27 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}s:5:"phone";s:11:"06302802985";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'szabolcsg'),
(35, 'update', '2014-06-24 05:33:46', '21', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 2, 'a:5:{s:12:"adminComment";s:1:"3";s:13:"returnAddress";s:1:"3";s:16:"returnAddressZip";s:1:"3";s:10:"returnDate";O:8:"DateTime":3:{s:4:"date";s:19:"2014-06-30 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}s:16:"assistanceStatus";a:1:{s:2:"id";i:11;}}', 'tmxadmin2'),
(36, 'update', '2014-06-24 05:52:18', '21', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 3, 'a:1:{s:12:"adminComment";s:58:"Kókai Balázs kérésére írom, hogy ezt most beszéltem";}', 'tmxadmin2'),
(37, 'update', '2014-06-24 05:53:12', '21', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 4, 'a:1:{s:16:"returnAddressZip";s:4:"2045";}', 'tmxadmin2'),
(38, 'update', '2014-06-24 05:53:33', '21', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 5, 'a:2:{s:12:"adminComment";s:72:"Kókai Balázs kérésére írom, hogy ezt most beszéltem és nem tudom";s:16:"returnAddressZip";s:4:"2050";}', 'tmxadmin2'),
(39, 'update', '2014-06-25 15:17:55', '16', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 2, 'a:2:{s:4:"city";s:8:"Budapest";s:16:"assistanceStatus";a:1:{s:2:"id";i:12;}}', 'tmxadmin2'),
(40, 'update', '2014-06-26 04:51:37', '20', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 4, 'a:3:{s:4:"city";s:8:"budapest";s:7:"address";s:12:"Ady Endre 21";s:16:"assistanceStatus";a:1:{s:2:"id";i:15;}}', 'tmxadmin2'),
(41, 'create', '2014-07-02 04:04:44', '22', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2051";s:4:"city";s:11:"Biatorbágy";s:7:"address";s:14:"Sándor utca 1";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-07 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}s:5:"phone";s:11:"06304241531";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'Egervarik4'),
(42, 'create', '2014-07-02 06:07:04', '23', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"1212";s:4:"city";s:4:"bsad";s:7:"address";s:4:"dasd";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-23 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}s:5:"phone";s:5:"dsada";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'tmxuser1'),
(43, 'update', '2014-07-02 06:07:24', '23', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 2, 'a:1:{s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-24 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}}', 'tmxuser1'),
(44, 'create', '2014-07-02 09:20:42', '24', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"1221";s:4:"city";s:2:"bp";s:7:"address";s:10:"ecseri út";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-07 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}s:5:"phone";s:9:"202587845";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'polgarzs'),
(45, 'update', '2014-07-02 09:24:08', '15', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 2, 'a:3:{s:4:"city";s:2:"bp";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2009-01-02 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}s:16:"assistanceStatus";a:1:{s:2:"id";i:12;}}', 'tmxadmin2'),
(46, 'update', '2014-07-02 09:25:30', '15', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 3, 'a:1:{s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-07 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}}', 'tmxadmin2'),
(47, 'update', '2014-07-02 09:25:48', '15', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 4, 'a:1:{s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-04 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}}', 'tmxadmin2'),
(48, 'update', '2014-07-03 07:18:33', '22', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 2, 'a:1:{s:16:"assistanceStatus";a:1:{s:2:"id";i:11;}}', 'tmxadmin2'),
(49, 'update', '2014-07-03 07:19:45', '22', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 3, 'a:1:{s:16:"assistanceStatus";a:1:{s:2:"id";i:13;}}', 'tmxadmin2'),
(50, 'update', '2014-07-03 07:19:53', '22', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 4, 'a:1:{s:16:"assistanceStatus";a:1:{s:2:"id";i:15;}}', 'tmxadmin2'),
(51, 'update', '2014-07-03 07:20:15', '22', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 5, 'a:4:{s:12:"adminComment";s:6:"ttzutr";s:13:"returnAddress";s:11:"Biatorbágy";s:16:"returnAddressZip";s:4:"2051";s:10:"returnDate";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-09 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}}', 'tmxadmin2'),
(52, 'create', '2014-07-03 07:34:22', '25', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2051";s:4:"city";s:11:"Biatorbágy";s:7:"address";s:14:"Viola utca 24.";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-07 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}s:5:"phone";s:11:"06304241531";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'Egervarik4'),
(55, 'create', '2014-07-11 08:49:45', '26', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:3:"sas";s:4:"city";s:5:"asasa";s:7:"address";s:3:"sas";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-14 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}s:5:"phone";s:3:"sas";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'tmxuser1'),
(56, 'remove', '2014-07-11 08:49:53', '26', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 2, 'N;', 'tmxuser1'),
(57, 'create', '2014-07-11 09:54:38', '27', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2049";s:4:"city";s:6:"diósd";s:7:"address";s:9:"nbvcxsrsz";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-15 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}s:5:"phone";s:9:"205554433";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'polgarzs'),
(58, 'update', '2014-07-11 09:57:31', '27', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 2, 'a:1:{s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-17 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}}', 'tmxadmin2'),
(59, 'update', '2014-07-11 09:57:45', '27', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 3, 'a:1:{s:16:"assistanceStatus";a:1:{s:2:"id";i:11;}}', 'tmxadmin2'),
(60, 'create', '2014-07-11 16:25:12', '28', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2030";s:4:"city";s:4:"érd";s:7:"address";s:10:"emília 21";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-14 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:16:"America/New_York";}s:5:"phone";s:7:"3333433";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'szabolcsg'),
(61, 'create', '2014-07-25 10:45:24', '29', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2051";s:4:"city";s:11:"Biatorbágy";s:7:"address";s:15:"Sándor utca 1.";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-07-28 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:13:"Europe/Berlin";}s:5:"phone";s:11:"06304241531";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'szarkane2011'),
(62, 'create', '2014-07-31 14:54:07', '30', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2030";s:4:"city";s:4:"Érd";s:7:"address";s:19:"Arany János utca 9";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-08-06 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:13:"Europe/Berlin";}s:5:"phone";s:6:"343434";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'szabolcsg'),
(63, 'create', '2014-07-31 14:55:51', '31', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2030";s:4:"city";s:4:"Érd";s:7:"address";s:9:"Arany J 9";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-08-07 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:13:"Europe/Berlin";}s:5:"phone";s:17:"86923983672897342";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'szabolcsg'),
(64, 'create', '2014-08-01 10:34:08', '32', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2040";s:4:"city";s:8:"Budaörs";s:7:"address";s:7:"Kinizsi";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-08-07 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:13:"Europe/Berlin";}s:5:"phone";s:6:"165165";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'szabolcsg'),
(65, 'create', '2014-08-05 16:21:56', '33', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2051";s:4:"city";s:11:"Biatorbágy";s:7:"address";s:15:"Sándor utca 1.";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-08-08 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:13:"Europe/Berlin";}s:5:"phone";s:11:"06304241531";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'szarkane2011'),
(66, 'create', '2014-08-05 16:44:13', '34', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2035";s:4:"city";s:4:"Érd";s:7:"address";s:15:"Emília utca 45";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-08-14 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:13:"Europe/Berlin";}s:5:"phone";s:7:"3423423";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'szabolcsg'),
(67, 'create', '2014-08-08 10:01:37', '35', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"0000";s:4:"city";s:2:"bg";s:7:"address";s:14:"gegaegíagvghz";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-08-11 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:13:"Europe/Berlin";}s:5:"phone";s:19:"0000000000000000000";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'polgarzs'),
(68, 'create', '2014-08-12 16:09:07', '36', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:6:"uié.j";s:4:"city";s:5:"vhl,h";s:7:"address";s:5:"hvlvh";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-08-15 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:13:"Europe/Berlin";}s:5:"phone";s:2:"lv";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'polgarzs'),
(69, 'create', '2014-08-13 11:18:07', '37', 'ServiceCrm\\AssistanceServiceBundle\\Entity\\RequiringCourierUser', 1, 'a:11:{s:12:"adminComment";N;s:13:"returnAddress";N;s:16:"returnAddressZip";N;s:10:"returnDate";N;s:3:"zip";s:4:"2040";s:4:"city";s:8:"budaörs";s:7:"address";s:13:"kinizsi u 2/b";s:4:"date";O:8:"DateTime":3:{s:4:"date";s:19:"2014-08-18 00:00:00";s:13:"timezone_type";i:3;s:8:"timezone";s:13:"Europe/Berlin";}s:5:"phone";s:14:"06-20-364-2845";s:10:"assistance";a:1:{s:2:"id";i:1;}s:16:"assistanceStatus";a:1:{s:2:"id";i:9;}}', 'soos.brigitta');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `ext_translations`
--

CREATE TABLE `ext_translations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `object_class` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `field` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `foreign_key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lookup_unique_idx` (`locale`,`object_class`,`field`,`foreign_key`),
  KEY `translations_lookup_idx` (`locale`,`object_class`,`foreign_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `file`
--

CREATE TABLE `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `metadata` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:json_array)',
  `size` int(11) NOT NULL,
  `mtime` datetime NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `temp` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `file_model` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `file_relation`
--

CREATE TABLE `file_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_id` int(11) DEFAULT NULL,
  `acl_object_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D3D9C8D493CB796C` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `fos_user_group`
--

CREATE TABLE `fos_user_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_583D1F3E5E237E06` (`name`),
  UNIQUE KEY `UNIQ_583D1F3E989D9B62` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- A tábla adatainak kiíratása `fos_user_group`
--

INSERT INTO `fos_user_group` (`id`, `name`, `roles`, `slug`) VALUES
(3, 'Ügyfél', 'a:4:{i:0;s:9:"ROLE_USER";i:1;s:10:"ROLE_ADMIN";i:2;s:6:"CLIENT";i:3;s:11:"ROLE_CLIENT";}', 'ugyfel'),
(5, 'Szolgaltató', 'a:6:{i:0;s:10:"ROLE_ADMIN";i:1;s:9:"ROLE_USER";i:2;s:6:"SONATA";i:3;s:8:"PROVIDER";i:4;s:8:"SUPPLIER";i:5;s:13:"ROLE_SUPPLIER";}', 'szolgaltato'),
(6, 'Szolgáltató admin', 'a:1:{i:0;s:19:"ROLE_SUPPLIER_ADMIN";}', 'szolgaltato_admin');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `fos_user_user`
--

CREATE TABLE `fos_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `firstname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `biography` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `locale` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `twitter_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `gplus_uid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gplus_data` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `two_step_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_zip` varchar(127) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_address` varchar(127) COLLATE utf8_unicode_ci DEFAULT NULL,
  `clients_active_supplier_id` int(11) DEFAULT NULL,
  `postal_city` varchar(127) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C560D76192FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_C560D761A0D96FBF` (`email_canonical`),
  KEY `IDX_C560D7612ADD6D8C` (`supplier_id`),
  KEY `IDX_C560D761AF338E6` (`clients_active_supplier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=72 ;

--
-- A tábla adatainak kiíratása `fos_user_user`
--

INSERT INTO `fos_user_user` (`id`, `supplier_id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `locked`, `expired`, `expires_at`, `confirmation_token`, `password_requested_at`, `roles`, `credentials_expired`, `credentials_expire_at`, `created_at`, `updated_at`, `date_of_birth`, `firstname`, `lastname`, `website`, `biography`, `gender`, `locale`, `timezone`, `phone`, `facebook_uid`, `facebook_name`, `facebook_data`, `twitter_uid`, `twitter_name`, `twitter_data`, `gplus_uid`, `gplus_name`, `gplus_data`, `token`, `two_step_code`, `postal_zip`, `postal_address`, `clients_active_supplier_id`, `postal_city`) VALUES
(1, NULL, 'admin', 'admin', 'admin@email.hu', 'admin@email.hu', 1, 'eqyfxbt0cu0wgwk4k80wsgw0wsggw84', '3BtjUYY0GotnsfLRzld8F2T/GGtnBt5wwtG/BXnV6Th7YeL02/sn+nB+YZz1cyz+yDAHS+RJNP9CLqIrCoUqXw==', '2014-08-22 16:51:33', 0, 0, NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', 0, NULL, '2014-05-26 09:03:39', '2014-08-22 16:51:33', '1899-06-05 00:00:00', 'Admin', 'admin', NULL, NULL, 'm', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, '2040', 'Kinizsi utca 2/B', NULL, 'Budaörs'),
(17, 1, 'tmx_mobile', 'tmx_mobile', 'tmxadmin1@www.hu', 'tmxadmin1@www.hu', 1, 'q5jls18zl3k8k0wssw0kwkww8sw4w4s', 'LF3GambTQ0vFCFQMVrbONleQOFG9aR6VxnVQI8Bd5hOOC//P6SPHv4zsQCnYxw5tBcRDAQL0+t/nYH18nhTdNA==', '2014-08-19 09:47:03', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2014-06-22 15:44:02', '2014-08-22 11:01:57', NULL, 'tmxadmin1', 'tmxadmin1', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 1, 'tmx_mobile_admin', 'tmx_mobile_admin', 'tmxadmin2@www.hu', 'tmxadmin2@www.hu', 1, 'l6ct6wu6mlsscssg4gc0ccs8gscsc0k', 'Rx/HFdouKd6pIky5rZJQlZuBg2oiEzSDG/VqzaVmAQA1eSWElRO/SN52BIB8SyfYOLxkhwt+eEfnzElNEo6/yQ==', '2014-07-11 09:55:58', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2014-06-22 15:44:29', '2014-08-22 11:03:38', NULL, 'tmxadmin2', 'tmxadmin2', NULL, NULL, 'u', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, NULL, NULL, NULL, NULL),
(19, NULL, 'test_user', 'test_user', 'tmxuser1@www.hu', 'tmxuser1@www.hu', 1, '4pycmtcrcgiscw48g4s840o4g8wggw0', '1HszUU537nfmUsw3a0ntTbhjUh+ioKg7XCDeUQnKhn/V54AloB/cXW8MQ6pzXxT8I5TESDJ+dgjzvgvTMMfuGA==', '2014-07-11 10:45:06', 0, 0, NULL, NULL, NULL, 'a:0:{}', 0, NULL, '2014-06-22 15:47:46', '2014-08-22 10:59:15', '1988-01-08 00:00:00', 'asdf', 'asdf', NULL, NULL, 'f', NULL, NULL, NULL, NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'null', NULL, NULL, 'asdf', 'asdfasdf', 1, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `fos_user_user_group`
--

CREATE TABLE `fos_user_user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`),
  KEY `IDX_B3C77447A76ED395` (`user_id`),
  KEY `IDX_B3C77447FE54D947` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `fos_user_user_group`
--

INSERT INTO `fos_user_user_group` (`user_id`, `group_id`) VALUES
(17, 5),
(18, 6),
(19, 3);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `manufacturer`
--

CREATE TABLE `manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- A tábla adatainak kiíratása `manufacturer`
--

INSERT INTO `manufacturer` (`id`, `created`, `updated`, `name`, `phone`, `email`, `address`) VALUES
(1, '2014-05-26 09:04:38', '2014-05-26 09:04:38', 'Lenovo', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `media__gallery`
--

CREATE TABLE `media__gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `default_format` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `media__gallery_media`
--

CREATE TABLE `media__gallery_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) DEFAULT NULL,
  `media_id` int(11) DEFAULT NULL,
  `position` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_80D4C5414E7AF8F` (`gallery_id`),
  KEY `IDX_80D4C541EA9FDD75` (`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `media__media`
--

CREATE TABLE `media__media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `enabled` tinyint(1) NOT NULL,
  `provider_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_status` int(11) NOT NULL,
  `provider_reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `provider_metadata` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:json)',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `length` decimal(10,0) DEFAULT NULL,
  `content_type` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_size` int(11) DEFAULT NULL,
  `copyright` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `context` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdn_is_flushable` tinyint(1) DEFAULT NULL,
  `cdn_flush_at` datetime DEFAULT NULL,
  `cdn_status` int(11) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `modelCode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `serial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imei` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `serial_check` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guarantee` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guaranteePlus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `guaranteeExpiryDate` datetime DEFAULT NULL,
  `buyPlace` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `buyDate` datetime NOT NULL,
  `productName` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `productType_id` int(11) DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D34A04AD3D0AE6DC` (`manufacturer`),
  KEY `IDX_D34A04ADA76ED395` (`user_id`),
  KEY `IDX_D34A04AD91BE1328` (`productType_id`),
  KEY `IDX_D34A04AD85564492` (`create_user_id`),
  KEY `IDX_D34A04AD2ADD6D8C` (`supplier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=74 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `product_media`
--

CREATE TABLE `product_media` (
  `product_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  PRIMARY KEY (`product_id`,`media_id`),
  KEY `IDX_CB70DA504584665A` (`product_id`),
  KEY `IDX_CB70DA50EA9FDD75` (`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `product_type`
--

CREATE TABLE `product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `formService` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- A tábla adatainak kiíratása `product_type`
--

INSERT INTO `product_type` (`id`, `formService`, `name`) VALUES
(1, NULL, 'notebook'),
(2, NULL, 'Tablet');

-- --------------------------------------------------------

--
-- Tábla szerkezet: `requiring_courier`
--

CREATE TABLE `requiring_courier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `assistance_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `accessories` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `error_description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `replacement_unit` tinyint(1) DEFAULT NULL,
  `zip` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `admin_comment` longtext COLLATE utf8_unicode_ci,
  `return_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `return_address_zip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `return_date` datetime DEFAULT NULL,
  `assistance_status_id` int(11) DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias_uniqid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A8CFEE524584665A` (`product_id`),
  KEY `IDX_A8CFEE52A76ED395` (`user_id`),
  KEY `IDX_A8CFEE527096529A` (`assistance_id`),
  KEY `IDX_A8CFEE528B74A071` (`assistance_status_id`),
  KEY `IDX_A8CFEE5285564492` (`create_user_id`),
  KEY `IDX_A8CFEE522ADD6D8C` (`supplier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=38 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `requiring_courier_media`
--

CREATE TABLE `requiring_courier_media` (
  `assistance_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  PRIMARY KEY (`assistance_id`,`media_id`),
  KEY `IDX_CFF36F5A7096529A` (`assistance_id`),
  KEY `IDX_CFF36F5AEA9FDD75` (`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `requiring_courier_status`
--

CREATE TABLE `requiring_courier_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_F9AD4E13989D9B62` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tábla szerkezet: `supplier`
--

CREATE TABLE `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- A tábla adatainak kiíratása `supplier`
--

INSERT INTO `supplier` (`id`, `name`, `phone`, `email`, `is_public`) VALUES
(1, 'TMX Mobile', '234', 'dasda@asd.hu', 1),
(2, 'Vodafone', '321', '1dasdas@asd.hu', 0);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `suppliermanufacturer_producttype`
--

CREATE TABLE `suppliermanufacturer_producttype` (
  `suppliermanufacturer_id` int(11) NOT NULL,
  `producttype_id` int(11) NOT NULL,
  PRIMARY KEY (`suppliermanufacturer_id`,`producttype_id`),
  KEY `IDX_DBE1956FD3486331` (`suppliermanufacturer_id`),
  KEY `IDX_DBE1956F5E032AB4` (`producttype_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `suppliermanufacturer_producttype`
--

INSERT INTO `suppliermanufacturer_producttype` (`suppliermanufacturer_id`, `producttype_id`) VALUES
(9, 1),
(9, 2);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `supplier_manufacturer`
--

CREATE TABLE `supplier_manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_576371852ADD6D8C` (`supplier_id`),
  KEY `IDX_57637185A23B42D` (`manufacturer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- A tábla adatainak kiíratása `supplier_manufacturer`
--

INSERT INTO `supplier_manufacturer` (`id`, `supplier_id`, `manufacturer_id`) VALUES
(9, 1, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `supplier_manufacture_assistance`
--

CREATE TABLE `supplier_manufacture_assistance` (
  `supplier_manufacturer_id` int(11) NOT NULL,
  `assistance_id` int(11) NOT NULL,
  PRIMARY KEY (`supplier_manufacturer_id`,`assistance_id`),
  KEY `IDX_9A5229063206459` (`supplier_manufacturer_id`),
  KEY `IDX_9A522907096529A` (`assistance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `supplier_manufacture_assistance`
--

INSERT INTO `supplier_manufacture_assistance` (`supplier_manufacturer_id`, `assistance_id`) VALUES
(9, 1);

-- --------------------------------------------------------

--
-- Tábla szerkezet: `user_supplier`
--

CREATE TABLE `user_supplier` (
  `user_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`supplier_id`),
  KEY `IDX_7BA7887EA76ED395` (`user_id`),
  KEY `IDX_7BA7887E2ADD6D8C` (`supplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `user_supplier`
--

INSERT INTO `user_supplier` (`user_id`, `supplier_id`) VALUES
(19, 1);

--
-- Megkötések a kiírt táblákhoz
--

--
-- Megkötések a táblához `acl_entries`
--
ALTER TABLE `acl_entries`
  ADD CONSTRAINT `FK_46C8B8063D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806DF9183C9` FOREIGN KEY (`security_identity_id`) REFERENCES `acl_security_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_46C8B806EA000B10` FOREIGN KEY (`class_id`) REFERENCES `acl_classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `acl_object_identities`
--
ALTER TABLE `acl_object_identities`
  ADD CONSTRAINT `FK_9407E54977FA751A` FOREIGN KEY (`parent_object_identity_id`) REFERENCES `acl_object_identities` (`id`);

--
-- Megkötések a táblához `acl_object_identity_ancestors`
--
ALTER TABLE `acl_object_identity_ancestors`
  ADD CONSTRAINT `FK_825DE2993D9AB4A6` FOREIGN KEY (`object_identity_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_825DE299C671CEA1` FOREIGN KEY (`ancestor_id`) REFERENCES `acl_object_identities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Megkötések a táblához `assistance_assistance_status`
--
ALTER TABLE `assistance_assistance_status`
  ADD CONSTRAINT `FK_B84613BD7096529A` FOREIGN KEY (`assistance_id`) REFERENCES `assistance` (`id`),
  ADD CONSTRAINT `FK_B84613BD8B74A071` FOREIGN KEY (`assistance_status_id`) REFERENCES `assistance_status` (`id`);

--
-- Megkötések a táblához `file_relation`
--
ALTER TABLE `file_relation`
  ADD CONSTRAINT `FK_D3D9C8D493CB796C` FOREIGN KEY (`file_id`) REFERENCES `file` (`id`);

--
-- Megkötések a táblához `fos_user_user`
--
ALTER TABLE `fos_user_user`
  ADD CONSTRAINT `FK_C560D7612ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`),
  ADD CONSTRAINT `FK_C560D761AF338E6` FOREIGN KEY (`clients_active_supplier_id`) REFERENCES `supplier` (`id`);

--
-- Megkötések a táblához `fos_user_user_group`
--
ALTER TABLE `fos_user_user_group`
  ADD CONSTRAINT `FK_B3C77447A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_B3C77447FE54D947` FOREIGN KEY (`group_id`) REFERENCES `fos_user_group` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `media__gallery_media`
--
ALTER TABLE `media__gallery_media`
  ADD CONSTRAINT `FK_80D4C5414E7AF8F` FOREIGN KEY (`gallery_id`) REFERENCES `media__gallery` (`id`),
  ADD CONSTRAINT `FK_80D4C541EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

--
-- Megkötések a táblához `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_D34A04AD2ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`),
  ADD CONSTRAINT `FK_D34A04AD3D0AE6DC` FOREIGN KEY (`manufacturer`) REFERENCES `manufacturer` (`id`),
  ADD CONSTRAINT `FK_D34A04AD85564492` FOREIGN KEY (`create_user_id`) REFERENCES `fos_user_user` (`id`),
  ADD CONSTRAINT `FK_D34A04AD91BE1328` FOREIGN KEY (`productType_id`) REFERENCES `product_type` (`id`),
  ADD CONSTRAINT `FK_D34A04ADA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`);

--
-- Megkötések a táblához `product_media`
--
ALTER TABLE `product_media`
  ADD CONSTRAINT `FK_CB70DA504584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `FK_CB70DA50EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

--
-- Megkötések a táblához `requiring_courier`
--
ALTER TABLE `requiring_courier`
  ADD CONSTRAINT `FK_A8CFEE522ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`),
  ADD CONSTRAINT `FK_A8CFEE524584665A` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  ADD CONSTRAINT `FK_A8CFEE527096529A` FOREIGN KEY (`assistance_id`) REFERENCES `assistance` (`id`),
  ADD CONSTRAINT `FK_A8CFEE5285564492` FOREIGN KEY (`create_user_id`) REFERENCES `fos_user_user` (`id`),
  ADD CONSTRAINT `FK_A8CFEE528B74A071` FOREIGN KEY (`assistance_status_id`) REFERENCES `assistance_status` (`id`),
  ADD CONSTRAINT `FK_A8CFEE52A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`);

--
-- Megkötések a táblához `requiring_courier_media`
--
ALTER TABLE `requiring_courier_media`
  ADD CONSTRAINT `FK_CFF36F5A7096529A` FOREIGN KEY (`assistance_id`) REFERENCES `requiring_courier` (`id`),
  ADD CONSTRAINT `FK_CFF36F5AEA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media__media` (`id`);

--
-- Megkötések a táblához `suppliermanufacturer_producttype`
--
ALTER TABLE `suppliermanufacturer_producttype`
  ADD CONSTRAINT `FK_DBE1956F5E032AB4` FOREIGN KEY (`producttype_id`) REFERENCES `product_type` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_DBE1956FD3486331` FOREIGN KEY (`suppliermanufacturer_id`) REFERENCES `supplier_manufacturer` (`id`) ON DELETE CASCADE;

--
-- Megkötések a táblához `supplier_manufacturer`
--
ALTER TABLE `supplier_manufacturer`
  ADD CONSTRAINT `FK_576371852ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`),
  ADD CONSTRAINT `FK_57637185A23B42D` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturer` (`id`);

--
-- Megkötések a táblához `supplier_manufacture_assistance`
--
ALTER TABLE `supplier_manufacture_assistance`
  ADD CONSTRAINT `FK_9A5229063206459` FOREIGN KEY (`supplier_manufacturer_id`) REFERENCES `supplier_manufacturer` (`id`),
  ADD CONSTRAINT `FK_9A522907096529A` FOREIGN KEY (`assistance_id`) REFERENCES `assistance` (`id`);

--
-- Megkötések a táblához `user_supplier`
--
ALTER TABLE `user_supplier`
  ADD CONSTRAINT `FK_7BA7887E2ADD6D8C` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_7BA7887EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user_user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
